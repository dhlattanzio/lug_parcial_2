﻿using BE;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class MP_Derivacion : Mapper<BE.Derivacion>
    {
        private Acceso acceso = Acceso.instancia;
        private readonly MP_Turno mpTurno = new MP_Turno();
        private readonly MP_Especialidad mpEspecialidad = new MP_Especialidad();

        public override int Borrar(Derivacion obj)
        {
            return -1;
        }

        public override int Editar(Derivacion obj)
        {
            return -1;
        }

        public override int Insertar(Derivacion obj)
        {
            List<SqlParameter> parametros = new List<SqlParameter>();
            parametros.Add(acceso.CrearParametro("@idTurno", obj.Turno.Id));
            parametros.Add(acceso.CrearParametro("@idEspecialidad", obj.Especialidad.Id));
            parametros.Add(acceso.CrearParametro("@fecha", obj.Fecha.ToString()));
            return acceso.Escribir("INSERTAR_DERIVACION", parametros);
        }

        public override List<Derivacion> Listar()
        {
            List<BE.Derivacion> derivaciones = new List<BE.Derivacion>();
            List<BE.Turno> turnos = mpTurno.Listar();
            List<BE.Especialidad> especialidades = mpEspecialidad.Listar();

            DataTable tabla = acceso.Leer("LISTAR_DERIVACION");
            foreach (DataRow registro in tabla.Rows)
            {
                BE.Derivacion der = new BE.Derivacion();
                der.Id = int.Parse(registro["id"].ToString());
                der.Turno = turnos.SingleOrDefault(x => x.Id == int.Parse(registro["idTurno"].ToString()));
                der.Especialidad = especialidades.SingleOrDefault(x => x.Id == int.Parse(registro["idEspecialidad"].ToString()));
                der.Fecha = DateTime.Parse(registro["fecha"].ToString());

                derivaciones.Add(der);
            }

            return derivaciones;
        }
    }
}

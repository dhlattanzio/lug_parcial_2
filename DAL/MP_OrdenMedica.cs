﻿using BE;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class MP_OrdenMedica : Mapper<BE.OrdenMedica>
    {
        private Acceso acceso = Acceso.instancia;
        private readonly MP_Medico mpMedico = new MP_Medico();
        private readonly MP_Paciente mpPaciente = new MP_Paciente();
        private readonly MP_Estudios mpEstudio = new MP_Estudios();


        public override int Borrar(OrdenMedica obj)
        {
            return -1;
        }

        public override int Editar(OrdenMedica obj)
        {
            List<SqlParameter> parametros = new List<SqlParameter>();
            parametros.Add(acceso.CrearParametro("@id", obj.Id));
            parametros.Add(acceso.CrearParametro("@idMedico", obj.Medico.Id));
            parametros.Add(acceso.CrearParametro("@idPaciente", obj.Paciente.Id));
            parametros.Add(acceso.CrearParametro("@idEstudio", obj.Estudio.Id));
            parametros.Add(acceso.CrearParametro("@fecha", obj.Fecha.ToString()));
            parametros.Add(acceso.CrearParametro("@fueHecho", obj.FueHecho));

            return acceso.Escribir("EDITAR_ORDEN", parametros);
        }

        public override int Insertar(OrdenMedica obj)
        {
            List<SqlParameter> parametros = new List<SqlParameter>();
            parametros.Add(acceso.CrearParametro("@idMedico", obj.Medico.Id));
            parametros.Add(acceso.CrearParametro("@idPaciente", obj.Paciente.Id));
            parametros.Add(acceso.CrearParametro("@idEstudio", obj.Estudio.Id));
            parametros.Add(acceso.CrearParametro("@fecha", obj.Fecha.ToString()));

            return acceso.Escribir("INSERTAR_ORDEN", parametros);
        }

        public override List<OrdenMedica> Listar()
        {
            List<BE.OrdenMedica> ordenes = new List<OrdenMedica>();
            List<BE.Medico> medicos = mpMedico.Listar();
            List<BE.Paciente> pacientes = mpPaciente.Listar();
            List<BE.Estudio> estudios = mpEstudio.Listar();

            DataTable tabla = acceso.Leer("LISTAR_ORDENES");
            foreach (DataRow registro in tabla.Rows)
            {
                BE.OrdenMedica orden = new BE.OrdenMedica();
                orden.Id = int.Parse(registro["id"].ToString());

                orden.Medico = medicos.SingleOrDefault(x => x.Id == int.Parse(registro["idMedico"].ToString()));
                orden.Paciente = pacientes.SingleOrDefault(x => x.Id == int.Parse(registro["idPaciente"].ToString()));
                orden.Estudio = estudios.SingleOrDefault(x => x.Id == int.Parse(registro["idEstudio"].ToString()));
                orden.Fecha = DateTime.Parse(registro["fecha"].ToString()); ;
                orden.FueHecho = bool.Parse(registro["fueHecho"].ToString());

                ordenes.Add(orden);
            }

            return ordenes;
        }
    }
}

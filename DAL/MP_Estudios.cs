﻿using BE;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class MP_Estudios : Mapper<BE.Estudio>
    {
        private Acceso acceso = Acceso.instancia;

        public override int Borrar(Estudio obj)
        {
            List<SqlParameter> parametros = new List<SqlParameter>();
            parametros.Add(acceso.CrearParametro("@id", obj.Id));
            return acceso.Escribir("BORRAR_ESTUDIO", parametros);
        }

        public override int Editar(Estudio obj)
        {
            List<SqlParameter> parametros = new List<SqlParameter>();
            parametros.Add(acceso.CrearParametro("@id", obj.Id));
            parametros.Add(acceso.CrearParametro("@nombre", obj.Nombre));
            parametros.Add(acceso.CrearParametro("@costo", obj.Costo));
            return acceso.Escribir("EDITAR_ESTUDIO", parametros);
        }

        public override int Insertar(Estudio obj)
        {
            List<SqlParameter> parametros = new List<SqlParameter>();
            parametros.Add(acceso.CrearParametro("@idEspecialidad", obj.IdEspecialidad));
            parametros.Add(acceso.CrearParametro("@nombre", obj.Nombre));
            parametros.Add(acceso.CrearParametro("@costo", obj.Costo));
            return acceso.Escribir("INSERTAR_ESTUDIO", parametros);
        }

        public override List<Estudio> Listar()
        {
            List<BE.Estudio> estudios = new List<BE.Estudio>();

            DataTable tabla = acceso.Leer("LISTAR_ESTUDIOS");
            foreach (DataRow registro in tabla.Rows)
            {
                BE.Estudio est = new BE.Estudio();
                est.Id = int.Parse(registro["id"].ToString());
                est.IdEspecialidad = int.Parse(registro["idEspecialidad"].ToString());
                est.Nombre = registro["nombre"].ToString();
                est.Costo = float.Parse(registro["costo"].ToString());

                estudios.Add(est);
            }

            return estudios;
        }
    }
}

﻿using BE;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class MP_HistoriaClinica : Mapper<BE.Historia>
    {
        private Acceso acceso = Acceso.instancia;
        private readonly MP_Medico mpMedico = new MP_Medico();
        private readonly MP_Paciente mpPaciente = new MP_Paciente();
        private readonly MP_Especialidad mpEspecialidad = new MP_Especialidad();

        public override int Borrar(Historia obj)
        {
            return -1;
        }

        public override int Editar(Historia obj)
        {
            return -1;
        }

        public override int Insertar(Historia obj)
        {
            List<SqlParameter> parametros = new List<SqlParameter>();
            parametros.Add(acceso.CrearParametro("@idMedico", obj.Medico.Id));
            parametros.Add(acceso.CrearParametro("@idPaciente", obj.Paciente.Id));
            parametros.Add(acceso.CrearParametro("@idEspecialidad", obj.Especialidad.Id));
            parametros.Add(acceso.CrearParametro("@fecha", obj.Fecha.ToString()));
            parametros.Add(acceso.CrearParametro("@costo", obj.Costo));
            parametros.Add(acceso.CrearParametro("@detalles", obj.Detalles));

            return acceso.Escribir("INSERTAR_HISTORIA", parametros);
        }

        public override List<Historia> Listar()
        {
            List<BE.Historia> historias = new List<Historia>();

            List<BE.Medico> medicos = mpMedico.Listar();
            List<BE.Paciente> pacientes = mpPaciente.Listar();
            List<BE.Especialidad> especialidades = mpEspecialidad.Listar();

            BE.Especialidad especialidadGeneral = new Especialidad();
            especialidadGeneral.Id = -1;
            especialidadGeneral.Nombre = "General";
            especialidadGeneral.Estudios = new List<Estudio>();
            especialidades.Add(especialidadGeneral);

            DataTable tabla = acceso.Leer("LISTAR_HISTORIA");
            foreach (DataRow registro in tabla.Rows)
            {
                BE.Historia historia = new BE.Historia();
                historia.Id = int.Parse(registro["id"].ToString());
                historia.Medico = medicos.SingleOrDefault(x => x.Id == int.Parse(registro["idMedico"].ToString()));
                historia.Paciente = pacientes.SingleOrDefault(x => x.Id == int.Parse(registro["idPaciente"].ToString()));
                historia.Especialidad = especialidades.SingleOrDefault(x => x.Id == int.Parse(registro["idEspecialidad"].ToString()));
                historia.Fecha = DateTime.Parse(registro["fecha"].ToString());
                historia.Costo = float.Parse(registro["costo"].ToString());
                historia.Detalles = registro["detalles"].ToString();

                historias.Add(historia);
            }

            return historias;
        }
    }
}

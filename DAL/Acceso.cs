﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace DAL
{
    internal class Acceso
    {
        public static Acceso instancia = new Acceso();
        private SqlConnection conexion;

        private void Abrir()
        {
            conexion = new SqlConnection();
            conexion.ConnectionString = @"Initial Catalog=LUG_Lattanzio_Parcial2; Data Source=.\SQLEXPRESS; Integrated Security=SSPI;";
            conexion.Open();
        }

        private void Cerrar()
        {
            conexion.Close();
            conexion.Dispose();
            conexion = null;
            GC.Collect();
        }

        private SqlCommand CrearComando(string nombre, List<SqlParameter> parametos = null)
        {
            SqlCommand comando = new SqlCommand();
            comando.CommandText = nombre;
            comando.CommandType = CommandType.StoredProcedure;
            comando.Connection = conexion;

            if (parametos != null && parametos.Count > 0)
            {
                comando.Parameters.AddRange(parametos.ToArray());
            }

            return comando;
        }

        private SqlParameter CrearParametro(string nombre, object valor, DbType tipo)
        {
            SqlParameter parametro = new SqlParameter(nombre, valor);
            parametro.DbType = tipo;
            return parametro;
        }

        public SqlParameter CrearParametro(string nombre, string valor)
        {
            return CrearParametro(nombre, valor, DbType.String);
        }

        public SqlParameter CrearParametro(string nombre, int valor)
        {
            return CrearParametro(nombre, valor, DbType.Int32);
        }

        public SqlParameter CrearParametro(string nombre, bool valor)
        {
            return CrearParametro(nombre, valor, DbType.Boolean);
        }

        public SqlParameter CrearParametro(string nombre, float valor)
        {
            return CrearParametro(nombre, valor, DbType.Double);
        }

        public int Escribir(string nombre, List<SqlParameter> parametros = null)
        {
            int filasAfectadas;

            Abrir();
            if (conexion != null && conexion.State == ConnectionState.Open)
            {
                SqlCommand comando = CrearComando(nombre, parametros);
                try
                {
                    filasAfectadas = comando.ExecuteNonQuery();
                }
                catch(Exception e)
                {
                    Console.WriteLine(e.Message);
                    filasAfectadas = -1;
                }

            } else
            {
                filasAfectadas = -2;
            }
            Cerrar();

            return filasAfectadas;
        }

        public DataTable Leer(string nombre, List<SqlParameter> parametros = null)
        {
            Abrir();

            DataTable tabla = new DataTable();
            SqlDataAdapter adaptador = new SqlDataAdapter();

            adaptador.SelectCommand = CrearComando(nombre, parametros);
            adaptador.Fill(tabla);

            Cerrar();
            return tabla;
        }
    }
}

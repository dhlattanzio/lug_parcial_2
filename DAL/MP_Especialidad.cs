﻿using BE;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class MP_Especialidad : Mapper<BE.Especialidad>
    {
        private Acceso acceso = Acceso.instancia;
        private MP_Estudios mpEstudios = new MP_Estudios();

        public override int Borrar(Especialidad obj)
        {
            List<SqlParameter> parametros = new List<SqlParameter>();
            parametros.Add(acceso.CrearParametro("@id", obj.Id));
            return acceso.Escribir("BORRAR_ESPECIALIDAD", parametros);
        }

        public override int Editar(Especialidad obj)
        {
            List<SqlParameter> parametros = new List<SqlParameter>();
            parametros.Add(acceso.CrearParametro("@id", obj.Id));
            parametros.Add(acceso.CrearParametro("@nombre", obj.Nombre));
            return acceso.Escribir("EDITAR_ESPECIALIDAD", parametros);
        }

        public override int Insertar(Especialidad obj)
        {
            List<SqlParameter> parametros = new List<SqlParameter>();
            parametros.Add(acceso.CrearParametro("@nombre", obj.Nombre));
            int res = acceso.Escribir("INSERTAR_ESPECIALIDAD", parametros);

            int id = Listar().Where(x => x.Nombre.ToLower() == obj.Nombre.ToLower()).First().Id;

            foreach(Estudio estudio in obj.Estudios)
            {
                estudio.IdEspecialidad = id;
                mpEstudios.Insertar(estudio);
            }

            return res;
        }

        public override List<Especialidad> Listar()
        {
            List<BE.Estudio> estudios = mpEstudios.Listar();

            List<BE.Especialidad> especialidades = new List<BE.Especialidad>();

            DataTable tabla = acceso.Leer("LISTAR_ESPECIALIDADES");
            foreach (DataRow registro in tabla.Rows)
            {
                BE.Especialidad esp = new BE.Especialidad();
                esp.Id = int.Parse(registro["id"].ToString());
                esp.Nombre = registro["nombre"].ToString();
                esp.Estudios = estudios.Where(e => e.IdEspecialidad == esp.Id).ToList();

                especialidades.Add(esp);
            }

            return especialidades;
        }
    }
}

﻿using BE;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class MP_Paciente : Mapper<BE.Paciente>
    {
        private Acceso acceso = Acceso.instancia;

        public override List<BE.Paciente> Listar()
        {
            List<BE.Paciente> pacientes = new List<Paciente>();

            DataTable tabla = acceso.Leer("LISTAR_PACIENTES");
            foreach (DataRow registro in tabla.Rows)
            {
                BE.Paciente p = new BE.Paciente();
                p.Id = int.Parse(registro["id"].ToString());
                p.Nombre = registro["nombre"].ToString();
                p.Apellido = registro["apellido"].ToString();
                p.Dni = int.Parse(registro["dni"].ToString());
                p.FechaNacimiento = DateTime.Parse(registro["fechaNacimiento"].ToString());

                pacientes.Add(p);
            }

            return pacientes;
        }

        public override int Borrar(Paciente obj)
        {
            List<SqlParameter> parametros = new List<SqlParameter>();
            parametros.Add(acceso.CrearParametro("@id", obj.Id));
            return acceso.Escribir("BORRAR_PACIENTE", parametros);
        }

        public override int Editar(Paciente obj)
        {
            List<SqlParameter> parametros = new List<SqlParameter>();
            parametros.Add(acceso.CrearParametro("@id", obj.Id));
            parametros.Add(acceso.CrearParametro("@nombre", obj.Nombre));
            parametros.Add(acceso.CrearParametro("@apellido", obj.Apellido));
            parametros.Add(acceso.CrearParametro("@dni", obj.Dni));
            parametros.Add(acceso.CrearParametro("@fechaNacimiento", obj.FechaNacimiento.ToString()));
            return acceso.Escribir("EDITAR_PACIENTE", parametros);
        }

        public override int Insertar(Paciente obj)
        {
            List<SqlParameter> parametros = new List<SqlParameter>();
            parametros.Add(acceso.CrearParametro("@nombre", obj.Nombre));
            parametros.Add(acceso.CrearParametro("@apellido", obj.Apellido));
            parametros.Add(acceso.CrearParametro("@dni", obj.Dni));
            parametros.Add(acceso.CrearParametro("@fechaNacimiento", obj.FechaNacimiento.ToString()));
            return acceso.Escribir("INSERTAR_PACIENTE", parametros);
        }
    }
}

﻿using BE;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class MP_Turno : Mapper<BE.Turno>
    {
        private Acceso acceso = Acceso.instancia;
        private readonly DAL.MP_Medico mpMedico = new MP_Medico();
        private readonly DAL.MP_Paciente mpPaciente = new MP_Paciente();
        private readonly DAL.MP_Especialidad mpEspecialidad = new MP_Especialidad();

        public override int Borrar(Turno obj)
        {
            List<SqlParameter> parametros = new List<SqlParameter>();
            parametros.Add(acceso.CrearParametro("@id", obj.Id));
            return acceso.Escribir("BORRAR_TURNO", parametros);
        }

        public override int Editar(Turno obj)
        {
            List<SqlParameter> parametros = new List<SqlParameter>();
            parametros.Add(acceso.CrearParametro("@id", obj.Id));
            parametros.Add(acceso.CrearParametro("@idMedico", obj.Medico.Id));
            parametros.Add(acceso.CrearParametro("@idPaciente", obj.Paciente.Id));
            parametros.Add(acceso.CrearParametro("@idEspecialidad", obj.Especialidad.Id));
            parametros.Add(acceso.CrearParametro("@fecha", obj.Fecha.ToString()));
            parametros.Add(acceso.CrearParametro("@numTurno", obj.NumeroTurno));
            parametros.Add(acceso.CrearParametro("@fueAtendido", obj.FueAtendido));
            return acceso.Escribir("EDITAR_TURNO", parametros);
        }

        public override int Insertar(Turno obj)
        {
            List<SqlParameter> parametros = new List<SqlParameter>();
            parametros.Add(acceso.CrearParametro("@idMedico", obj.Medico.Id));
            parametros.Add(acceso.CrearParametro("@idPaciente", obj.Paciente.Id));
            parametros.Add(acceso.CrearParametro("@idEspecialidad", obj.Especialidad.Id));
            parametros.Add(acceso.CrearParametro("@fecha", obj.Fecha.ToString()));
            parametros.Add(acceso.CrearParametro("@numTurno", obj.NumeroTurno));
            return acceso.Escribir("INSERTAR_TURNO", parametros);
        }

        public override List<Turno> Listar()
        {
            List<BE.Turno> turnos = new List<Turno>();
            List<BE.Medico> medicos = mpMedico.Listar();
            List<BE.Paciente> pacientes = mpPaciente.Listar();
            List<BE.Especialidad> especialidades = mpEspecialidad.Listar();

            BE.Especialidad especialidadGeneral = new Especialidad();
            especialidadGeneral.Id = -1;
            especialidadGeneral.Nombre = "General";
            especialidadGeneral.Estudios = new List<Estudio>();

            DataTable tabla = acceso.Leer("LISTAR_TURNO");
            foreach (DataRow registro in tabla.Rows)
            {
                BE.Turno turno = new BE.Turno();
                turno.Id = int.Parse(registro["id"].ToString());
                turno.Medico = medicos.SingleOrDefault(x => x.Id == int.Parse(registro["idMedico"].ToString()));
                turno.Paciente = pacientes.SingleOrDefault(x => x.Id == int.Parse(registro["idPaciente"].ToString()));
                turno.Especialidad = especialidades.SingleOrDefault(x => x.Id == int.Parse(registro["idEspecialidad"].ToString())) ?? especialidadGeneral;
                turno.Fecha = DateTime.Parse(registro["fecha"].ToString());
                turno.FueAtendido = bool.Parse(registro["fueAtendido"].ToString());
                turno.NumeroTurno = int.Parse(registro["numTurno"].ToString());

                if (turno.Medico == null || turno.Paciente == null) continue;
                turnos.Add(turno);
            }

            return turnos;
        }
    }
}

﻿using BE;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class MP_Medico : Mapper<BE.Medico>
    {
        private Acceso acceso = Acceso.instancia;
        private MP_Especialidad mpEspecialidad = new MP_Especialidad();

        public override int Borrar(Medico obj)
        {
            List<SqlParameter> parametros = new List<SqlParameter>();
            parametros.Add(acceso.CrearParametro("@id", obj.Id));
            return acceso.Escribir("BORRAR_MEDICO", parametros);
        }

        public override int Editar(Medico obj)
        {
            List<SqlParameter> parametros = new List<SqlParameter>();
            parametros.Add(acceso.CrearParametro("@nombre", obj.Nombre));
            parametros.Add(acceso.CrearParametro("@apellido", obj.Apellido));
            parametros.Add(acceso.CrearParametro("@cuil", obj.Cuil));
            parametros.Add(acceso.CrearParametro("@costo", obj.Costo));
            parametros.Add(acceso.CrearParametro("@fechaNacimiento", obj.FechaNacimiento.ToString()));
            parametros.Add(acceso.CrearParametro("@esMedicoGuardia", obj.EsMedicoDeGuardia));

            acceso.Escribir("EDITAR_MEDICO", parametros);

            parametros = new List<SqlParameter>();
            parametros.Add(acceso.CrearParametro("@id", obj.Id));
            acceso.Escribir("BORRAR_MEDICO_ESPECIALIDADES", parametros);

            foreach (BE.Especialidad especialidad in obj.Especialidades)
            {
                AgregarEspecialidad(obj, especialidad);
            }

            return 1;
        }

        public override int Insertar(Medico obj)
        {
            List<SqlParameter> parametros = new List<SqlParameter>();
            parametros.Add(acceso.CrearParametro("@nombre", obj.Nombre));
            parametros.Add(acceso.CrearParametro("@apellido", obj.Apellido));
            parametros.Add(acceso.CrearParametro("@cuil", obj.Cuil));
            parametros.Add(acceso.CrearParametro("@costo", obj.Costo));
            parametros.Add(acceso.CrearParametro("@fechaNacimiento", obj.FechaNacimiento.ToString()));
            parametros.Add(acceso.CrearParametro("@esMedicoGuardia", obj.EsMedicoDeGuardia));

            acceso.Escribir("INSERTAR_MEDICO", parametros);

            parametros.Clear();
            parametros.Add(acceso.CrearParametro("@cuil", obj.Cuil));
            int medicoId = int.Parse(acceso.Leer("OBTENER_MEDICO_POR_CUIL", parametros).Rows[0]["id"].ToString());

            obj.Id = medicoId;
            foreach(BE.Especialidad especialidad in obj.Especialidades)
            {
                AgregarEspecialidad(obj, especialidad);
            }

            return 1;
        }

        public int AgregarEspecialidad(Medico obj, BE.Especialidad especialidad)
        {
            List<SqlParameter> parametros = new List<SqlParameter>();
            parametros.Add(acceso.CrearParametro("@idMedico", obj.Id));
            parametros.Add(acceso.CrearParametro("@idEspecialidad", especialidad.Id));

            return acceso.Escribir("AGREGAR_MEDICO_ESPECIALIDAD", parametros);
        }

        public override List<Medico> Listar()
        {
            List<BE.Medico> medicos = new List<BE.Medico>();
            List<BE.Especialidad> especialidades = mpEspecialidad.Listar();
            
            BE.Especialidad especialidadGeneral = new Especialidad();
            especialidadGeneral.Id = -1;
            especialidadGeneral.Nombre = "General";
            especialidadGeneral.Estudios = new List<Estudio>();

            Dictionary<int, List<int>> dictEspecialidades = new Dictionary<int, List<int>>();
            DataTable tabla = acceso.Leer("LISTAR_MEDICO_ESPECIALIDAD");
            foreach (DataRow registro in tabla.Rows)
            {
                int medicoId = int.Parse(registro["idMedico"].ToString());
                int especialidadId = int.Parse(registro["idEspecialidad"].ToString());

                List<int> temp = dictEspecialidades.ContainsKey(medicoId) ?
                    dictEspecialidades[medicoId] :
                    new List<int>();
                temp.Add(especialidadId);

                dictEspecialidades[medicoId] = temp;
            }

            tabla = acceso.Leer("LISTAR_MEDICO");
            foreach (DataRow registro in tabla.Rows)
            {
                BE.Medico medico = new BE.Medico();
                medico.Id = int.Parse(registro["id"].ToString());
                medico.Nombre = registro["nombre"].ToString();
                medico.Apellido = registro["apellido"].ToString();
                medico.Cuil = int.Parse(registro["cuil"].ToString());
                medico.Costo = float.Parse(registro["costo"].ToString());
                medico.FechaNacimiento = DateTime.Parse(registro["fechaNacimiento"].ToString());
                medico.EsMedicoDeGuardia = bool.Parse(registro["esMedicoGuardia"].ToString());

                medico.Especialidades = new List<Especialidad>();
                medico.Especialidades.Add(especialidadGeneral);
                if (dictEspecialidades.ContainsKey(medico.Id))
                {
                    dictEspecialidades[medico.Id].ForEach(x => 
                    {
                        if (especialidades.Any(e => e.Id == x))
                        {
                            medico.Especialidades.Add(especialidades.Single(y => y.Id == x));
                        }
                    });
                }

                medicos.Add(medico);
            }

            return medicos;
        }
    }
}

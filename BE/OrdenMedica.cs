﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BE
{
    public class OrdenMedica
    {
        public int Id { get; set; }
        public Medico Medico { get; set; }
        public Paciente Paciente { get; set; }
        public Estudio Estudio { get; set; }
        public DateTime Fecha { get; set; }
        public bool FueHecho { get; set; }
    }
}

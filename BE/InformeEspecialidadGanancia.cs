﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BE
{
    public class InformeEspecialidadGanancia
    {
        public Especialidad Especialidad { get; set; }
        public float Ganancia { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BE
{
    public class Medico
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public int Cuil { get; set; }
        public DateTime FechaNacimiento { get; set; }
        public float Costo { get; set; }
        public List<Especialidad> Especialidades { get; set; }
        public bool EsMedicoDeGuardia { get; set; }

        public override string ToString()
        {
            return $"{Nombre} {Apellido}" + (EsMedicoDeGuardia ? " (G)" : "");
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BE
{
    public class Estudio
    {
        public int Id { get; set; }
        public int IdEspecialidad { get; set; }
        public string Nombre { get; set; }
        public float Costo { get; set; }

        public override string ToString()
        {
            return Nombre + ": $" + Costo;
        }
    }
}

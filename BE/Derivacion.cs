﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BE
{
    public class Derivacion
    {
        public int Id { get; set; }
        public BE.Turno Turno { get; set; }
        public BE.Especialidad Especialidad { get; set; }
        public DateTime Fecha { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BE
{
    public class InformeEspecialidadCantidad
    {
        public Especialidad Especialidad { get; set; }
        public int Cantidad { get; set; }
    }
}

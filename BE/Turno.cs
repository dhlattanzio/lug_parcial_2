﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BE
{
    public class Turno
    {
        public int Id { get; set; }
        public BE.Medico Medico { get; set; }
        public BE.Paciente Paciente { get; set; }
        public BE.Especialidad Especialidad { get; set; }
        public DateTime Fecha { get; set; }
        public int NumeroTurno { get; set; }
        public bool FueAtendido { get; set; }
    }
}

﻿
namespace DiegoLattanzio_LUG_Parcial_2
{
    partial class FormCargaEspecialidades
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridViewEspecialidades = new System.Windows.Forms.DataGridView();
            this.buttonEliminarEspecialidad = new System.Windows.Forms.Button();
            this.buttonAgregarEspecialidad = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label7 = new System.Windows.Forms.Label();
            this.textBoxEstudioCosto = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.textBoxEstudioNombre = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.buttonEliminarEstudio = new System.Windows.Forms.Button();
            this.buttonAgregarEstudio = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.dataGridViewEstudios = new System.Windows.Forms.DataGridView();
            this.textBoxNombreEspecialidad = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.textBoxDetallesNombre = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.dataGridViewDetallesEstudios = new System.Windows.Forms.DataGridView();
            this.textBoxDetallesId = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewEspecialidades)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewEstudios)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewDetallesEstudios)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridViewEspecialidades
            // 
            this.dataGridViewEspecialidades.AllowUserToAddRows = false;
            this.dataGridViewEspecialidades.AllowUserToDeleteRows = false;
            this.dataGridViewEspecialidades.AllowUserToResizeColumns = false;
            this.dataGridViewEspecialidades.AllowUserToResizeRows = false;
            this.dataGridViewEspecialidades.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewEspecialidades.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewEspecialidades.Location = new System.Drawing.Point(12, 37);
            this.dataGridViewEspecialidades.MultiSelect = false;
            this.dataGridViewEspecialidades.Name = "dataGridViewEspecialidades";
            this.dataGridViewEspecialidades.ReadOnly = true;
            this.dataGridViewEspecialidades.RowHeadersVisible = false;
            this.dataGridViewEspecialidades.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders;
            this.dataGridViewEspecialidades.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewEspecialidades.Size = new System.Drawing.Size(444, 201);
            this.dataGridViewEspecialidades.TabIndex = 0;
            this.dataGridViewEspecialidades.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewEspecialidades_CellClick);
            this.dataGridViewEspecialidades.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridViewEspecialidades_CellMouseClick);
            // 
            // buttonEliminarEspecialidad
            // 
            this.buttonEliminarEspecialidad.Location = new System.Drawing.Point(636, 385);
            this.buttonEliminarEspecialidad.Name = "buttonEliminarEspecialidad";
            this.buttonEliminarEspecialidad.Size = new System.Drawing.Size(140, 53);
            this.buttonEliminarEspecialidad.TabIndex = 1;
            this.buttonEliminarEspecialidad.Text = "Eliminar";
            this.buttonEliminarEspecialidad.UseVisualStyleBackColor = true;
            this.buttonEliminarEspecialidad.Click += new System.EventHandler(this.buttonEliminarEspecialidad_Click);
            // 
            // buttonAgregarEspecialidad
            // 
            this.buttonAgregarEspecialidad.Location = new System.Drawing.Point(490, 385);
            this.buttonAgregarEspecialidad.Name = "buttonAgregarEspecialidad";
            this.buttonAgregarEspecialidad.Size = new System.Drawing.Size(140, 53);
            this.buttonAgregarEspecialidad.TabIndex = 2;
            this.buttonAgregarEspecialidad.Text = "Agregar";
            this.buttonAgregarEspecialidad.UseVisualStyleBackColor = true;
            this.buttonAgregarEspecialidad.Click += new System.EventHandler(this.buttonAgregarEspecialidad_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.textBoxEstudioCosto);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.textBoxEstudioNombre);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.buttonEliminarEstudio);
            this.groupBox1.Controls.Add(this.buttonAgregarEstudio);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.dataGridViewEstudios);
            this.groupBox1.Controls.Add(this.textBoxNombreEspecialidad);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Location = new System.Drawing.Point(477, 37);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(311, 333);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Nueva Especialidad";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(27, 301);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(13, 13);
            this.label7.TabIndex = 12;
            this.label7.Text = "$";
            // 
            // textBoxEstudioCosto
            // 
            this.textBoxEstudioCosto.Location = new System.Drawing.Point(46, 298);
            this.textBoxEstudioCosto.Name = "textBoxEstudioCosto";
            this.textBoxEstudioCosto.Size = new System.Drawing.Size(124, 20);
            this.textBoxEstudioCosto.TabIndex = 11;
            this.textBoxEstudioCosto.Text = "0";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(23, 282);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(34, 13);
            this.label6.TabIndex = 10;
            this.label6.Text = "Costo";
            // 
            // textBoxEstudioNombre
            // 
            this.textBoxEstudioNombre.Location = new System.Drawing.Point(24, 254);
            this.textBoxEstudioNombre.Name = "textBoxEstudioNombre";
            this.textBoxEstudioNombre.Size = new System.Drawing.Size(144, 20);
            this.textBoxEstudioNombre.TabIndex = 9;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(21, 238);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(81, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Nombre estudio";
            // 
            // buttonEliminarEstudio
            // 
            this.buttonEliminarEstudio.Location = new System.Drawing.Point(190, 289);
            this.buttonEliminarEstudio.Name = "buttonEliminarEstudio";
            this.buttonEliminarEstudio.Size = new System.Drawing.Size(94, 36);
            this.buttonEliminarEstudio.TabIndex = 7;
            this.buttonEliminarEstudio.Text = "Eliminar";
            this.buttonEliminarEstudio.UseVisualStyleBackColor = true;
            this.buttonEliminarEstudio.Click += new System.EventHandler(this.buttonEliminarEstudio_Click);
            // 
            // buttonAgregarEstudio
            // 
            this.buttonAgregarEstudio.Location = new System.Drawing.Point(190, 245);
            this.buttonAgregarEstudio.Name = "buttonAgregarEstudio";
            this.buttonAgregarEstudio.Size = new System.Drawing.Size(94, 36);
            this.buttonAgregarEstudio.TabIndex = 6;
            this.buttonAgregarEstudio.Text = "Agregar";
            this.buttonAgregarEstudio.UseVisualStyleBackColor = true;
            this.buttonAgregarEstudio.Click += new System.EventHandler(this.buttonAgregarEstudio_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(23, 67);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(47, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Estudios";
            // 
            // dataGridViewEstudios
            // 
            this.dataGridViewEstudios.AllowUserToAddRows = false;
            this.dataGridViewEstudios.AllowUserToDeleteRows = false;
            this.dataGridViewEstudios.AllowUserToResizeColumns = false;
            this.dataGridViewEstudios.AllowUserToResizeRows = false;
            this.dataGridViewEstudios.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewEstudios.Location = new System.Drawing.Point(24, 100);
            this.dataGridViewEstudios.MultiSelect = false;
            this.dataGridViewEstudios.Name = "dataGridViewEstudios";
            this.dataGridViewEstudios.RowHeadersVisible = false;
            this.dataGridViewEstudios.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewEstudios.Size = new System.Drawing.Size(260, 125);
            this.dataGridViewEstudios.TabIndex = 4;
            // 
            // textBoxNombreEspecialidad
            // 
            this.textBoxNombreEspecialidad.Location = new System.Drawing.Point(153, 31);
            this.textBoxNombreEspecialidad.Name = "textBoxNombreEspecialidad";
            this.textBoxNombreEspecialidad.Size = new System.Drawing.Size(131, 20);
            this.textBoxNombreEspecialidad.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(21, 34);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(106, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Nombre especialidad";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(13, 13);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(118, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Lista de Especialidades";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.textBoxDetallesNombre);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.dataGridViewDetallesEstudios);
            this.groupBox2.Controls.Add(this.textBoxDetallesId);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Location = new System.Drawing.Point(16, 244);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(440, 194);
            this.groupBox2.TabIndex = 5;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Detalles";
            // 
            // textBoxDetallesNombre
            // 
            this.textBoxDetallesNombre.Location = new System.Drawing.Point(167, 41);
            this.textBoxDetallesNombre.Name = "textBoxDetallesNombre";
            this.textBoxDetallesNombre.ReadOnly = true;
            this.textBoxDetallesNombre.Size = new System.Drawing.Size(131, 20);
            this.textBoxDetallesNombre.TabIndex = 9;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(164, 25);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(44, 13);
            this.label8.TabIndex = 8;
            this.label8.Text = "Nombre";
            // 
            // dataGridViewDetallesEstudios
            // 
            this.dataGridViewDetallesEstudios.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewDetallesEstudios.Location = new System.Drawing.Point(20, 75);
            this.dataGridViewDetallesEstudios.MultiSelect = false;
            this.dataGridViewDetallesEstudios.Name = "dataGridViewDetallesEstudios";
            this.dataGridViewDetallesEstudios.ReadOnly = true;
            this.dataGridViewDetallesEstudios.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewDetallesEstudios.Size = new System.Drawing.Size(401, 107);
            this.dataGridViewDetallesEstudios.TabIndex = 7;
            // 
            // textBoxDetallesId
            // 
            this.textBoxDetallesId.Location = new System.Drawing.Point(20, 41);
            this.textBoxDetallesId.Name = "textBoxDetallesId";
            this.textBoxDetallesId.ReadOnly = true;
            this.textBoxDetallesId.Size = new System.Drawing.Size(131, 20);
            this.textBoxDetallesId.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(17, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(18, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "ID";
            // 
            // FormCargaEspecialidades
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.buttonAgregarEspecialidad);
            this.Controls.Add(this.buttonEliminarEspecialidad);
            this.Controls.Add(this.dataGridViewEspecialidades);
            this.Name = "FormCargaEspecialidades";
            this.Text = "FormCargaEspecialidades";
            this.Load += new System.EventHandler(this.FormCargaEspecialidades_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewEspecialidades)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewEstudios)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewDetallesEstudios)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridViewEspecialidades;
        private System.Windows.Forms.Button buttonEliminarEspecialidad;
        private System.Windows.Forms.Button buttonAgregarEspecialidad;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox textBoxEstudioNombre;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button buttonEliminarEstudio;
        private System.Windows.Forms.Button buttonAgregarEstudio;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DataGridView dataGridViewEstudios;
        private System.Windows.Forms.TextBox textBoxNombreEspecialidad;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox textBoxEstudioCosto;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox textBoxDetallesNombre;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.DataGridView dataGridViewDetallesEstudios;
        private System.Windows.Forms.TextBox textBoxDetallesId;
        private System.Windows.Forms.Label label1;
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DiegoLattanzio_LUG_Parcial_2
{
    public partial class FormSolicitarTurno : Form
    {
        private BLL.Paciente bllPaciente;
        private BLL.Medico bllMedico;
        private BLL.Turno bllTurnos;

        private readonly List<ItemListaTurno> horariosValidos = new List<ItemListaTurno>();

        private List<BE.Turno> listaTurnos;

        public FormSolicitarTurno()
        {
            InitializeComponent();
            StartPosition = FormStartPosition.CenterParent;

            for(int i=9;i<=18;i++)
            {
                ItemListaTurno item = new ItemListaTurno();
                item.index = i - 9;
                item.valor = i + ":00 hs";
                horariosValidos.Add(item);
            }
        }

        private void FormSolicitarTurno_Load(object sender, EventArgs e)
        {
            bllPaciente = new BLL.Paciente();
            bllMedico = new BLL.Medico();
            bllTurnos = new BLL.Turno();

            monthCalendar1.MinDate = DateTime.Now;

            CargarTurnos();
            CargarPacientes();
            CargarMedicos();
            ActualizarEspecialidades();
            ActualizarHoras();
        }

        private void CargarTurnos()
        {
            listaTurnos = bllTurnos.Listar();
        }

        private void CargarPacientes()
        {
            dataGridViewPaciente.DataSource = bllPaciente.Listar();
        }
        
        private void CargarMedicos()
        {
            dataGridViewMedico.DataSource = bllMedico.ListarMedicos().ToList();
        }

        private void ActualizarEspecialidades()
        {
            BE.Medico medico = (BE.Medico)dataGridViewMedico.CurrentRow?.DataBoundItem;
            if (medico != null)
            {
                comboBoxEspecialidad.DataSource = null;
                comboBoxEspecialidad.DataSource = medico.Especialidades;
            }
        }

        private void ActualizarHoras()
        {
            BE.Medico medico = (BE.Medico)dataGridViewMedico.CurrentRow?.DataBoundItem;
            if (medico != null)
            {
                DateTime fecha = monthCalendar1.SelectionRange.Start;

                labelGuardia.Visible = medico.EsMedicoDeGuardia;
                labelGuardia2.Visible = medico.EsMedicoDeGuardia;

                List<ItemListaTurno> listaHoras = new List<ItemListaTurno>(horariosValidos);

                if (!medico.EsMedicoDeGuardia)
                {
                    listaTurnos.Where(x => x.Medico?.Id == medico.Id)
                        .Where(x => x.Fecha == fecha)
                        .ToList().ForEach(turno =>
                        {
                            listaHoras.Remove(listaHoras.SingleOrDefault(x => x.index == turno.NumeroTurno));
                        }
                    );
                }

                comboBoxHoraTurno.DataSource = null;
                comboBoxHoraTurno.DataSource = listaHoras;

                labelSinHorarios.Visible = (listaHoras.Count == 0);
            }
        }

        private void SolicitarTurno(BE.Paciente paciente, BE.Medico medico, BE.Especialidad especialidad, ItemListaTurno turno)
        {
            DateTime fecha = monthCalendar1.SelectionRange.Start;

            BE.Turno nuevoTurno = new BE.Turno();
            nuevoTurno.Medico = medico;
            nuevoTurno.Paciente = paciente;
            nuevoTurno.Especialidad = especialidad;
            nuevoTurno.Fecha = fecha;
            nuevoTurno.NumeroTurno = turno.index;
            bllTurnos.Grabar(nuevoTurno);

            MessageBox.Show("Turno agregado con exito!");

            CargarTurnos();
            ActualizarHoras();
        }

        private void dataGridViewMedico_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            ActualizarEspecialidades();
            ActualizarHoras();
        }

        private void monthCalendar1_DateChanged(object sender, DateRangeEventArgs e)
        {
            ActualizarHoras();
        }

        private void buttonPedirTurno_Click(object sender, EventArgs e)
        {
            BE.Paciente paciente = (BE.Paciente)dataGridViewPaciente.CurrentRow?.DataBoundItem;
            BE.Medico medico = (BE.Medico)dataGridViewMedico.CurrentRow?.DataBoundItem;
            ItemListaTurno turno = (ItemListaTurno)comboBoxHoraTurno.SelectedItem;
            BE.Especialidad especialidad = (BE.Especialidad)comboBoxEspecialidad.SelectedItem;

            if (paciente == null)
            {
                MessageBox.Show("Selecciona un paciente de la lista.");
            } else if (medico == null)
            {
                MessageBox.Show("Selecciona un medico de la lista.");
            } else if (turno == null)
            {
                MessageBox.Show("Selecciona un turno de la lista.");
            } else if (especialidad == null) {
                MessageBox.Show("Selecciona una especializacion de la lista.");
            } else
            {
                SolicitarTurno(paciente, medico, especialidad, turno);
            }
        }

        class ItemListaTurno
        {
            public int index { get; set; }
            public string valor { get; set; }

            public override string ToString()
            {
                return valor;
            }
        }
    }
}

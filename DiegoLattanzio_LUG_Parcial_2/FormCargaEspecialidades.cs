﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DiegoLattanzio_LUG_Parcial_2
{
    public partial class FormCargaEspecialidades : Form
    {
        private BLL.Especialidad bllEspecialidad;

        public FormCargaEspecialidades()
        {
            InitializeComponent();
            StartPosition = FormStartPosition.CenterScreen;
        }

        private void FormCargaEspecialidades_Load(object sender, EventArgs e)
        {
            bllEspecialidad = new BLL.Especialidad();

            CargarEspecialidades();

            dataGridViewEstudios.DataSource = new BindingList<BE.Estudio>();
        }

        private void CargarEspecialidades()
        {
            dataGridViewEspecialidades.DataSource = bllEspecialidad.Listar();
        }

        private void CrearEspecialidad(string nombre, BindingList<BE.Estudio> estudios)
        {
            BE.Especialidad especialidad = new BE.Especialidad();
            especialidad.Nombre = nombre;
            especialidad.Estudios = new List<BE.Estudio>(estudios);

            if (!bllEspecialidad.Grabar(especialidad))
            {
                MessageBox.Show("Ocurrio un problema al crear la especialidad.");
            } else
            {
                CargarEspecialidades();
            }
        }

        private void EliminarEspecialidad(BE.Especialidad especialidad)
        {
            if (!bllEspecialidad.Borrar(especialidad))
            {
                MessageBox.Show("Ocurrio un problema al borrar la especialidad.");
            } else
            {
                CargarEspecialidades();
            }
        }

        private void CargarDetalles(BE.Especialidad especialidad)
        {
            textBoxDetallesId.Text = especialidad.Id.ToString();
            textBoxDetallesNombre.Text = especialidad.Nombre;
            dataGridViewDetallesEstudios.DataSource = null;
            dataGridViewDetallesEstudios.DataSource = especialidad.Estudios;
        }

        private void buttonAgregarEstudio_Click(object sender, EventArgs e)
        {
            string nombreEstudio = textBoxEstudioNombre.Text;
            float costoEstudio;
            bool exito = float.TryParse(textBoxEstudioCosto.Text, out costoEstudio);
            if (string.IsNullOrWhiteSpace(nombreEstudio))
            {
                MessageBox.Show("El nombre del estudio no es valido.");
            } else if (!exito)
            {
                MessageBox.Show("El costo del estudio no es valido.");
            } else
            {
                BE.Estudio estudio = new BE.Estudio();
                estudio.Nombre = nombreEstudio;
                estudio.Costo = costoEstudio;

                BindingList<BE.Estudio> lista = (BindingList<BE.Estudio>)dataGridViewEstudios.DataSource;
                lista.Add(estudio);

                textBoxEstudioNombre.Text = "";
                textBoxEstudioCosto.Text = "0";
            }
        }

        private void buttonEliminarEstudio_Click(object sender, EventArgs e)
        {
            BE.Estudio estudio = (BE.Estudio)dataGridViewEstudios.CurrentRow?.DataBoundItem;
            if (estudio != null)
            {
                BindingList<BE.Estudio> lista = (BindingList<BE.Estudio>)dataGridViewEstudios.DataSource;
                lista.Remove(estudio);
            } else
            {
                MessageBox.Show("No hay ningun estudio seleccionado.");
            }
        }

        private void buttonAgregarEspecialidad_Click(object sender, EventArgs e)
        {
            string nombreEspecialidad = textBoxNombreEspecialidad.Text;
            BindingList<BE.Estudio> listaEstudios = (BindingList<BE.Estudio>)dataGridViewEstudios.DataSource;

            List<BE.Especialidad> listaEspecialidades = 
                (List<BE.Especialidad>)dataGridViewEspecialidades.DataSource;

            if (string.IsNullOrWhiteSpace(nombreEspecialidad))
            {
                MessageBox.Show("El nombre de la especialidad no es valido.");
            }
            else if (listaEstudios.Count == 0)
            {
                MessageBox.Show("La especialidad debe tener al menos un estudio.");
            }
            else if (listaEspecialidades.Count(x => x.Nombre.ToLower() == nombreEspecialidad.ToLower()) > 0)
            {
                MessageBox.Show("Ya existe una especialidad con ese nombre");
            }
            else
            {
                CrearEspecialidad(nombreEspecialidad, listaEstudios);
                textBoxNombreEspecialidad.Text = "";
                listaEstudios.Clear();
            }
        }

        private void buttonEliminarEspecialidad_Click(object sender, EventArgs e)
        {
            BE.Especialidad especialidad = (BE.Especialidad)dataGridViewEspecialidades.CurrentRow?.DataBoundItem;
            if (especialidad != null)
            {
                EliminarEspecialidad(especialidad);
            }
            else
            {
                MessageBox.Show("No hay ninguna especialidad seleccionada.");
            }
        }

        private void dataGridViewEspecialidades_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            BE.Especialidad especialidad = (BE.Especialidad)dataGridViewEspecialidades.CurrentRow?.DataBoundItem;
            if (especialidad != null)
            {
                CargarDetalles(especialidad);
            }
        }

        private void dataGridViewEspecialidades_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DiegoLattanzio_LUG_Parcial_2
{
    public partial class FormCargaMedicos : Form
    {
        private BLL.Medico bllMedico;
        private BindingList<BE.Especialidad> listaEspecialidades = new BindingList<BE.Especialidad>();

        public FormCargaMedicos()
        {
            InitializeComponent();
            StartPosition = FormStartPosition.CenterScreen;
        }

        private void FormCargaMedicos_Load(object sender, EventArgs e)
        {
            bllMedico = new BLL.Medico();
            dataGridViewEspecialidades.DataSource = listaEspecialidades;
            CargarMedicos();
        }

        private void CargarMedicos()
        {
            dataGridViewMedicos.DataSource = null;
            dataGridViewMedicos.DataSource = bllMedico.ListarMedicos();
        }

        private void AgregarMedico(string nombre, string apellido, int cuil, float costo, 
            DateTime fechaNacimiento, bool esMedicoDeGuardia, List<BE.Especialidad> especialidades)
        {
            if (bllMedico.ListarMedicos().Any(x => x.Cuil == cuil))
            {
                MessageBox.Show("Ya existe un medico registrado con ese CUIL.");
                return;
            }

            BE.Medico medico= new BE.Medico();
            medico.Nombre = nombre;
            medico.Apellido = apellido;
            medico.Cuil = cuil;
            medico.Costo = costo;
            medico.FechaNacimiento = fechaNacimiento;
            medico.EsMedicoDeGuardia = esMedicoDeGuardia;
            medico.Especialidades = especialidades;

            bllMedico.Grabar(medico);
        }

        private void ModificarMedico(BE.Medico medico, string nombre, string apellido, int cuil, float costo,
            DateTime fechaNacimiento, bool esMedicoDeGuardia, List<BE.Especialidad> especialidades)
        {
            medico.Nombre = nombre;
            medico.Apellido = apellido;
            medico.Cuil = cuil;
            medico.Costo = costo;
            medico.FechaNacimiento = fechaNacimiento;
            medico.EsMedicoDeGuardia = esMedicoDeGuardia;
            medico.Especialidades = especialidades;

            bllMedico.Editar(medico);
        }

        private void BorrarMedico(BE.Medico medico)
        {
            bllMedico.Borrar(medico);
        }

        private void LimpiarCampos()
        {
            listaEspecialidades.Clear();
            CargarMedicos();
        }

        private void buttonAgregarEspecialidad_Click(object sender, EventArgs e)
        {
            DialogoSeleccionarEspecialidad dialogoAgregarEspecialidad = new DialogoSeleccionarEspecialidad(listaEspecialidades);
            DialogResult result = dialogoAgregarEspecialidad.ShowDialog();

            if (result == DialogResult.OK)
            {
                BE.Especialidad especialidad = dialogoAgregarEspecialidad.EspecialidadSeleccionada;

                if (listaEspecialidades.Any(x => x.Id == especialidad.Id))
                {
                    MessageBox.Show("El medico ya tiene esa especialidad.");
                } else
                {
                    listaEspecialidades.Add(especialidad);
                }
            }
        }

        private void buttonBorrarEspecialidad_Click(object sender, EventArgs e)
        {
            BE.Especialidad especialidad = (BE.Especialidad)dataGridViewEspecialidades.CurrentRow?.DataBoundItem;
            if (especialidad != null)
            {
                listaEspecialidades.Remove(especialidad);
            }
        }

        private void buttonAgregarMedico_Click(object sender, EventArgs e)
        {
            string nombre = textBoxNombre.Text;
            string apellido = textBoxApellido.Text;
            int cuil;
            bool cuilOk = int.TryParse(textBoxCuil.Text, out cuil);
            float costo;
            bool costoOk = float.TryParse(textBoxCosto.Text, out costo);
            DateTime fechaNacimiento = dateTimePickerFechaNacimiento.Value;
            bool esMedicoGuardia = checkBoxMedicoGuardia.Checked;

            if (string.IsNullOrWhiteSpace(nombre))
            {
                MessageBox.Show("El nombre no es valido.");
            } else if (string.IsNullOrWhiteSpace(apellido))
            {
                MessageBox.Show("El apellido no es valido.");
            } else if (!cuilOk)
            {
                MessageBox.Show("El CUIL no es valido.");
            } else if (!costoOk)
            {
                MessageBox.Show("El costo no es valido.");
            } else
            {
                AgregarMedico(nombre, apellido, cuil, costo, fechaNacimiento,
                    esMedicoGuardia, new List<BE.Especialidad>(listaEspecialidades));
                LimpiarCampos();
            }
        }

        private void buttonModificarMedico_Click(object sender, EventArgs e)
        {
            BE.Medico medico = (BE.Medico)dataGridViewMedicos.CurrentRow?.DataBoundItem;
            if (medico != null)
            {
                string nombre = textBoxNombre.Text;
                string apellido = textBoxApellido.Text;
                int cuil;
                bool cuilOk = int.TryParse(textBoxCuil.Text, out cuil);
                float costo;
                bool costoOk = float.TryParse(textBoxCosto.Text, out costo);
                DateTime fechaNacimiento = dateTimePickerFechaNacimiento.Value;
                bool esMedicoGuardia = checkBoxMedicoGuardia.Checked;

                if (string.IsNullOrWhiteSpace(nombre))
                {
                    MessageBox.Show("El nombre no es valido.");
                }
                else if (string.IsNullOrWhiteSpace(apellido))
                {
                    MessageBox.Show("El apellido no es valido.");
                }
                else if (!cuilOk)
                {
                    MessageBox.Show("El CUIL no es valido.");
                }
                else if (!costoOk)
                {
                    MessageBox.Show("El costo no es valido.");
                }
                else
                {
                    ModificarMedico(medico, nombre, apellido, cuil, costo, fechaNacimiento,
                        esMedicoGuardia, new List<BE.Especialidad>(listaEspecialidades));
                    LimpiarCampos();
                }
            }
        }

        private void buttonEliminarMedico_Click(object sender, EventArgs e)
        {
            BE.Medico medico = (BE.Medico)dataGridViewMedicos.CurrentRow?.DataBoundItem;
            if (medico != null)
            {
                BorrarMedico(medico);
                LimpiarCampos();
            }
        }

        private void dataGridViewMedicos_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            BE.Medico medico = (BE.Medico)dataGridViewMedicos.CurrentRow?.DataBoundItem;
            if (medico != null)
            {
                textBoxId.Text = medico.Id.ToString();
                textBoxNombre.Text = medico.Nombre;
                textBoxApellido.Text = medico.Apellido;
                textBoxCuil.Text = medico.Cuil.ToString();
                textBoxCosto.Text = medico.Costo.ToString();
                dateTimePickerFechaNacimiento.Value = medico.FechaNacimiento;
                checkBoxMedicoGuardia.Checked = medico.EsMedicoDeGuardia;

                listaEspecialidades.Clear();
                foreach(BE.Especialidad esp in medico.Especialidades) {
                    if (esp.Id>=0) listaEspecialidades.Add(esp);
                }
            }
        }
    }
}

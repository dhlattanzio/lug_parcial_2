﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DiegoLattanzio_LUG_Parcial_2
{
    public partial class DialogoHistoriaPaciente : Form
    {
        private BLL.HistoriaClinica bllHistoria;
        private readonly BE.Paciente paciente;
        private readonly BE.Especialidad filtroEspecialidad;

        public DialogoHistoriaPaciente(BE.Paciente paciente, BE.Especialidad filtroEspecialidad)
        {
            InitializeComponent();
            this.paciente = paciente;
            this.filtroEspecialidad = filtroEspecialidad;
            StartPosition = FormStartPosition.CenterParent;
        }

        private void DialogoHistoriaPaciente_Load(object sender, EventArgs e)
        {
            bllHistoria = new BLL.HistoriaClinica();
            ListarHistoriaClinica();
        }

        private void ListarHistoriaClinica()
        {
            List<BE.Historia> historias;
            if (filtroEspecialidad != null)
            {
                historias = bllHistoria.ObtenerHistoriasFiltradaEspecialidad(paciente, filtroEspecialidad);
            } else
            {
                historias= bllHistoria.ObtenerHistoriasFormaDescendienteFecha(paciente);
            }
            dataGridViewHistoriaClinica.DataSource = historias;
        }

        private void button1_Click(object sender, EventArgs e)
        {
        }
    }
}

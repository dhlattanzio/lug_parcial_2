﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DiegoLattanzio_LUG_Parcial_2
{
    public partial class FormCargaPacientes : Form
    {
        private BLL.Paciente bllPaciente;

        public FormCargaPacientes()
        {
            InitializeComponent();
            StartPosition = FormStartPosition.CenterScreen;
        }

        private void FormCargaPacientes_Load(object sender, EventArgs e)
        {
            bllPaciente = new BLL.Paciente();
            CargarListaPacientes();
        }

        private void CargarListaPacientes()
        {
            dataGridViewPacientes.DataSource = bllPaciente.Listar();
        }

        private void CrearPaciente(string nombre, string apellido, int dni, DateTime fechaNacimiento)
        {
            BE.Paciente paciente = new BE.Paciente();
            paciente.Nombre = nombre;
            paciente.Apellido = apellido;
            paciente.Dni = dni;
            paciente.FechaNacimiento = fechaNacimiento;

            if (bllPaciente.Grabar(paciente))
            {
                CargarListaPacientes();
            } else
            {
                MessageBox.Show("Ocurrio un problema al crear el paciente.");
            }
        }

        private void BorrarPaciente(BE.Paciente paciente)
        {
            if (bllPaciente.Borrar(paciente))
            {
                CargarListaPacientes();
            }
            else
            {
                MessageBox.Show("Ocurrio un problema al borrar el paciente.");
            }
        }

        private void ModificarPaciente(BE.Paciente paciente, string nombre, string apellido,
            int dni, DateTime fechaNacimiento)
        {
            BE.Paciente nuevo = new BE.Paciente();
            nuevo.Id = paciente.Id;
            nuevo.Nombre = nombre;
            nuevo.Apellido = apellido;
            nuevo.Dni = dni;
            nuevo.FechaNacimiento = fechaNacimiento;

            if (bllPaciente.Editar(nuevo))
            {
                CargarListaPacientes();
            }
            else
            {
                MessageBox.Show("Ocurrio un problema al modificar el paciente.");
            }
        }

        private void CargarDetalles(BE.Paciente paciente)
        {
            textBoxId.Text = paciente.Id.ToString();
            textBoxNombre.Text = paciente.Nombre;
            textBoxApellido.Text = paciente.Apellido;
            textBoxDni.Text = paciente.Dni.ToString();
        }

        private bool ValidarDatos(string nombre, string apellido, string dni)
        {
            int tmp;
            bool exito = int.TryParse(dni, out tmp);
            if (string.IsNullOrWhiteSpace(nombre))
            {
                MessageBox.Show("El nombre del paciente no es valido.");
            }
            else if (string.IsNullOrWhiteSpace(apellido))
            {
                MessageBox.Show("El apellido del paciente no es valido.");
            }
            else if (!exito)
            {
                MessageBox.Show("El DNI del paciente no es valido.");
            }
            else
            {
                return true;
            }
            return false;
        }

        private void LimpiarCampos()
        {
            textBoxId.Text = "";
            textBoxNombre.Text = "";
            textBoxApellido.Text = "";
            textBoxDni.Text = "";
        }

        private void buttonCrear_Click(object sender, EventArgs e)
        {
            string nombre = textBoxNombre.Text;
            string apellido = textBoxApellido.Text;
            if (ValidarDatos(nombre, apellido, textBoxDni.Text)) {
                int dni = int.Parse(textBoxDni.Text);

                List<BE.Paciente> pacientes = (List<BE.Paciente>)dataGridViewPacientes.DataSource;
                if (pacientes.Any(x => x.Dni == dni))
                {
                    MessageBox.Show("Ya existe un paciente con el mismo DNI.");
                } else
                {
                    CrearPaciente(nombre, apellido, dni, dateTimePicker1.Value);
                    LimpiarCampos();
                }
            }
        }

        private void buttonModificar_Click(object sender, EventArgs e)
        {
            BE.Paciente paciente = (BE.Paciente)dataGridViewPacientes.CurrentRow?.DataBoundItem;

            if (paciente != null)
            {
                string nombre = textBoxNombre.Text;
                string apellido = textBoxApellido.Text;
                if (ValidarDatos(nombre, apellido, textBoxDni.Text))
                {
                    int dni = int.Parse(textBoxDni.Text);
                    ModificarPaciente(paciente, nombre, apellido, dni, dateTimePicker1.Value);
                    LimpiarCampos();
                }
            }
        }

        private void buttonBorrar_Click(object sender, EventArgs e)
        {
            BE.Paciente paciente = (BE.Paciente)dataGridViewPacientes.CurrentRow?.DataBoundItem;

            if (paciente != null)
            {
                BorrarPaciente(paciente);
                LimpiarCampos();
            }
        }

        private void dataGridViewPacientes_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            BE.Paciente paciente = (BE.Paciente)dataGridViewPacientes.CurrentRow?.DataBoundItem;

            if (paciente != null)
            {
                CargarDetalles(paciente);
            }
        }
    }
}

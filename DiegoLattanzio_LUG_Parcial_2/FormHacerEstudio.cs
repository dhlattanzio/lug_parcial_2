﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DiegoLattanzio_LUG_Parcial_2
{
    public partial class FormHacerEstudio : Form
    {
        private BLL.OrdenMedica bllOrdenMedica;
        private BLL.HistoriaClinica bllHistoria;
        private BLL.Especialidad bllEspecialidad;

        public FormHacerEstudio()
        {
            InitializeComponent();
            StartPosition = FormStartPosition.CenterParent;
        }

        private void FormHacerEstudio_Load(object sender, EventArgs e)
        {
            bllOrdenMedica = new BLL.OrdenMedica();
            bllHistoria = new BLL.HistoriaClinica();
            bllEspecialidad = new BLL.Especialidad();
            ListarOrdenesMedicas();
        }

        private void ListarOrdenesMedicas()
        {
            dataGridView1.DataSource = bllOrdenMedica.Listar().Where(x => !x.FueHecho).ToList();
        }

        private void RegistrarResultado(BE.OrdenMedica orden, string resultado)
        {
            orden.FueHecho = true;
            bllOrdenMedica.Editar(orden);

            BE.Historia historia = new BE.Historia();
            historia.Medico = orden.Medico;
            historia.Paciente = orden.Paciente;
            historia.Especialidad = bllEspecialidad.ObtenerEspecialidad(orden.Estudio.IdEspecialidad);
            historia.Fecha = DateTime.Now;
            historia.Detalles = $"Estudio  {orden.Estudio.Nombre}: " + resultado;
            historia.Costo = orden.Estudio.Costo;
            bllHistoria.Grabar(historia);

            textBox1.Text = "";
            ListarOrdenesMedicas();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            BE.OrdenMedica orden = (BE.OrdenMedica)dataGridView1.CurrentRow?.DataBoundItem;
            if (orden!=null)
            {
                string detalles = textBox1.Text;
                if (string.IsNullOrWhiteSpace(detalles))
                {
                    MessageBox.Show("El campo de resultado no es valido.");
                } else
                {
                    RegistrarResultado(orden, detalles);
                }
            } else
            {
                MessageBox.Show("Selecciona una orden medica.");
            }
        }
    }
}

﻿
namespace DiegoLattanzio_LUG_Parcial_2
{
    partial class FormCargaMedicos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.buttonBorrarEspecialidad = new System.Windows.Forms.Button();
            this.buttonAgregarEspecialidad = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.dataGridViewEspecialidades = new System.Windows.Forms.DataGridView();
            this.checkBoxMedicoGuardia = new System.Windows.Forms.CheckBox();
            this.dateTimePickerFechaNacimiento = new System.Windows.Forms.DateTimePicker();
            this.label6 = new System.Windows.Forms.Label();
            this.textBoxCosto = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.textBoxCuil = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.textBoxApellido = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxNombre = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxId = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.dataGridViewMedicos = new System.Windows.Forms.DataGridView();
            this.buttonAgregarMedico = new System.Windows.Forms.Button();
            this.buttonModificarMedico = new System.Windows.Forms.Button();
            this.buttonEliminarMedico = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewEspecialidades)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewMedicos)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.buttonBorrarEspecialidad);
            this.groupBox1.Controls.Add(this.buttonAgregarEspecialidad);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.dataGridViewEspecialidades);
            this.groupBox1.Controls.Add(this.checkBoxMedicoGuardia);
            this.groupBox1.Controls.Add(this.dateTimePickerFechaNacimiento);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.textBoxCosto);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.textBoxCuil);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.textBoxApellido);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.textBoxNombre);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.textBoxId);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(462, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(326, 397);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Detalles";
            // 
            // buttonBorrarEspecialidad
            // 
            this.buttonBorrarEspecialidad.Location = new System.Drawing.Point(111, 359);
            this.buttonBorrarEspecialidad.Name = "buttonBorrarEspecialidad";
            this.buttonBorrarEspecialidad.Size = new System.Drawing.Size(100, 23);
            this.buttonBorrarEspecialidad.TabIndex = 32;
            this.buttonBorrarEspecialidad.Text = "Borrar Esp";
            this.buttonBorrarEspecialidad.UseVisualStyleBackColor = true;
            this.buttonBorrarEspecialidad.Click += new System.EventHandler(this.buttonBorrarEspecialidad_Click);
            // 
            // buttonAgregarEspecialidad
            // 
            this.buttonAgregarEspecialidad.Location = new System.Drawing.Point(11, 359);
            this.buttonAgregarEspecialidad.Name = "buttonAgregarEspecialidad";
            this.buttonAgregarEspecialidad.Size = new System.Drawing.Size(94, 23);
            this.buttonAgregarEspecialidad.TabIndex = 6;
            this.buttonAgregarEspecialidad.Text = "Agregar Esp";
            this.buttonAgregarEspecialidad.UseVisualStyleBackColor = true;
            this.buttonAgregarEspecialidad.Click += new System.EventHandler(this.buttonAgregarEspecialidad_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(12, 228);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(78, 13);
            this.label7.TabIndex = 31;
            this.label7.Text = "Especialidades";
            // 
            // dataGridViewEspecialidades
            // 
            this.dataGridViewEspecialidades.AllowUserToAddRows = false;
            this.dataGridViewEspecialidades.AllowUserToDeleteRows = false;
            this.dataGridViewEspecialidades.AllowUserToResizeRows = false;
            this.dataGridViewEspecialidades.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewEspecialidades.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewEspecialidades.Location = new System.Drawing.Point(11, 244);
            this.dataGridViewEspecialidades.MultiSelect = false;
            this.dataGridViewEspecialidades.Name = "dataGridViewEspecialidades";
            this.dataGridViewEspecialidades.ReadOnly = true;
            this.dataGridViewEspecialidades.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewEspecialidades.Size = new System.Drawing.Size(309, 109);
            this.dataGridViewEspecialidades.TabIndex = 30;
            // 
            // checkBoxMedicoGuardia
            // 
            this.checkBoxMedicoGuardia.AutoSize = true;
            this.checkBoxMedicoGuardia.Location = new System.Drawing.Point(15, 206);
            this.checkBoxMedicoGuardia.Name = "checkBoxMedicoGuardia";
            this.checkBoxMedicoGuardia.Size = new System.Drawing.Size(114, 17);
            this.checkBoxMedicoGuardia.TabIndex = 29;
            this.checkBoxMedicoGuardia.Text = "Medico de guardia";
            this.checkBoxMedicoGuardia.UseVisualStyleBackColor = true;
            // 
            // dateTimePickerFechaNacimiento
            // 
            this.dateTimePickerFechaNacimiento.Location = new System.Drawing.Point(11, 174);
            this.dateTimePickerFechaNacimiento.Name = "dateTimePickerFechaNacimiento";
            this.dateTimePickerFechaNacimiento.Size = new System.Drawing.Size(200, 20);
            this.dateTimePickerFechaNacimiento.TabIndex = 27;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 151);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(93, 13);
            this.label6.TabIndex = 26;
            this.label6.Text = "Fecha Nacimiento";
            // 
            // textBoxCosto
            // 
            this.textBoxCosto.Location = new System.Drawing.Point(79, 123);
            this.textBoxCosto.Name = "textBoxCosto";
            this.textBoxCosto.Size = new System.Drawing.Size(100, 20);
            this.textBoxCosto.TabIndex = 25;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 126);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(34, 13);
            this.label5.TabIndex = 24;
            this.label5.Text = "Costo";
            // 
            // textBoxCuil
            // 
            this.textBoxCuil.Location = new System.Drawing.Point(79, 97);
            this.textBoxCuil.Name = "textBoxCuil";
            this.textBoxCuil.Size = new System.Drawing.Size(100, 20);
            this.textBoxCuil.TabIndex = 23;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 100);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(31, 13);
            this.label4.TabIndex = 22;
            this.label4.Text = "CUIL";
            // 
            // textBoxApellido
            // 
            this.textBoxApellido.Location = new System.Drawing.Point(79, 71);
            this.textBoxApellido.Name = "textBoxApellido";
            this.textBoxApellido.Size = new System.Drawing.Size(100, 20);
            this.textBoxApellido.TabIndex = 21;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 74);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(44, 13);
            this.label2.TabIndex = 20;
            this.label2.Text = "Apellido";
            // 
            // textBoxNombre
            // 
            this.textBoxNombre.Location = new System.Drawing.Point(79, 45);
            this.textBoxNombre.Name = "textBoxNombre";
            this.textBoxNombre.Size = new System.Drawing.Size(100, 20);
            this.textBoxNombre.TabIndex = 19;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 48);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(44, 13);
            this.label3.TabIndex = 18;
            this.label3.Text = "Nombre";
            // 
            // textBoxId
            // 
            this.textBoxId.Location = new System.Drawing.Point(79, 19);
            this.textBoxId.Name = "textBoxId";
            this.textBoxId.ReadOnly = true;
            this.textBoxId.Size = new System.Drawing.Size(100, 20);
            this.textBoxId.TabIndex = 17;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(16, 13);
            this.label1.TabIndex = 16;
            this.label1.Text = "Id";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.dataGridViewMedicos);
            this.groupBox2.Location = new System.Drawing.Point(12, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(444, 347);
            this.groupBox2.TabIndex = 5;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Medicos";
            // 
            // dataGridViewMedicos
            // 
            this.dataGridViewMedicos.AllowUserToAddRows = false;
            this.dataGridViewMedicos.AllowUserToDeleteRows = false;
            this.dataGridViewMedicos.AllowUserToResizeRows = false;
            this.dataGridViewMedicos.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewMedicos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewMedicos.Location = new System.Drawing.Point(6, 16);
            this.dataGridViewMedicos.MultiSelect = false;
            this.dataGridViewMedicos.Name = "dataGridViewMedicos";
            this.dataGridViewMedicos.ReadOnly = true;
            this.dataGridViewMedicos.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewMedicos.Size = new System.Drawing.Size(430, 325);
            this.dataGridViewMedicos.TabIndex = 5;
            this.dataGridViewMedicos.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewMedicos_CellClick);
            // 
            // buttonAgregarMedico
            // 
            this.buttonAgregarMedico.Location = new System.Drawing.Point(41, 363);
            this.buttonAgregarMedico.Name = "buttonAgregarMedico";
            this.buttonAgregarMedico.Size = new System.Drawing.Size(126, 38);
            this.buttonAgregarMedico.TabIndex = 6;
            this.buttonAgregarMedico.Text = "Agregar";
            this.buttonAgregarMedico.UseVisualStyleBackColor = true;
            this.buttonAgregarMedico.Click += new System.EventHandler(this.buttonAgregarMedico_Click);
            // 
            // buttonModificarMedico
            // 
            this.buttonModificarMedico.Location = new System.Drawing.Point(173, 363);
            this.buttonModificarMedico.Name = "buttonModificarMedico";
            this.buttonModificarMedico.Size = new System.Drawing.Size(126, 38);
            this.buttonModificarMedico.TabIndex = 33;
            this.buttonModificarMedico.Text = "Modificar";
            this.buttonModificarMedico.UseVisualStyleBackColor = true;
            this.buttonModificarMedico.Click += new System.EventHandler(this.buttonModificarMedico_Click);
            // 
            // buttonEliminarMedico
            // 
            this.buttonEliminarMedico.Location = new System.Drawing.Point(305, 365);
            this.buttonEliminarMedico.Name = "buttonEliminarMedico";
            this.buttonEliminarMedico.Size = new System.Drawing.Size(126, 36);
            this.buttonEliminarMedico.TabIndex = 34;
            this.buttonEliminarMedico.Text = "Eliminar";
            this.buttonEliminarMedico.UseVisualStyleBackColor = true;
            this.buttonEliminarMedico.Click += new System.EventHandler(this.buttonEliminarMedico_Click);
            // 
            // FormCargaMedicos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 429);
            this.Controls.Add(this.buttonAgregarMedico);
            this.Controls.Add(this.buttonEliminarMedico);
            this.Controls.Add(this.buttonModificarMedico);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "FormCargaMedicos";
            this.Text = "FormCargaMedicos";
            this.Load += new System.EventHandler(this.FormCargaMedicos_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewEspecialidades)).EndInit();
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewMedicos)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DateTimePicker dateTimePickerFechaNacimiento;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBoxCosto;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBoxCuil;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBoxApellido;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxNombre;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBoxId;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DataGridView dataGridViewMedicos;
        private System.Windows.Forms.Button buttonBorrarEspecialidad;
        private System.Windows.Forms.Button buttonAgregarEspecialidad;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.DataGridView dataGridViewEspecialidades;
        private System.Windows.Forms.CheckBox checkBoxMedicoGuardia;
        private System.Windows.Forms.Button buttonAgregarMedico;
        private System.Windows.Forms.Button buttonModificarMedico;
        private System.Windows.Forms.Button buttonEliminarMedico;
    }
}
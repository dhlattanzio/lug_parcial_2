﻿
namespace DiegoLattanzio_LUG_Parcial_2
{
    partial class FormSolicitarTurno
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dataGridViewPaciente = new System.Windows.Forms.DataGridView();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.dataGridViewMedico = new System.Windows.Forms.DataGridView();
            this.buttonPedirTurno = new System.Windows.Forms.Button();
            this.monthCalendar1 = new System.Windows.Forms.MonthCalendar();
            this.label1 = new System.Windows.Forms.Label();
            this.comboBoxHoraTurno = new System.Windows.Forms.ComboBox();
            this.labelSinHorarios = new System.Windows.Forms.Label();
            this.comboBoxEspecialidad = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.labelGuardia = new System.Windows.Forms.Label();
            this.labelGuardia2 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewPaciente)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewMedico)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.dataGridViewPaciente);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(433, 209);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Paciente";
            // 
            // dataGridViewPaciente
            // 
            this.dataGridViewPaciente.AllowUserToAddRows = false;
            this.dataGridViewPaciente.AllowUserToDeleteRows = false;
            this.dataGridViewPaciente.AllowUserToResizeRows = false;
            this.dataGridViewPaciente.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewPaciente.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewPaciente.Location = new System.Drawing.Point(7, 20);
            this.dataGridViewPaciente.MultiSelect = false;
            this.dataGridViewPaciente.Name = "dataGridViewPaciente";
            this.dataGridViewPaciente.ReadOnly = true;
            this.dataGridViewPaciente.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewPaciente.Size = new System.Drawing.Size(420, 183);
            this.dataGridViewPaciente.TabIndex = 0;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.dataGridViewMedico);
            this.groupBox2.Location = new System.Drawing.Point(12, 227);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(433, 209);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Medico";
            // 
            // dataGridViewMedico
            // 
            this.dataGridViewMedico.AllowUserToAddRows = false;
            this.dataGridViewMedico.AllowUserToDeleteRows = false;
            this.dataGridViewMedico.AllowUserToResizeRows = false;
            this.dataGridViewMedico.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewMedico.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewMedico.Location = new System.Drawing.Point(6, 19);
            this.dataGridViewMedico.MultiSelect = false;
            this.dataGridViewMedico.Name = "dataGridViewMedico";
            this.dataGridViewMedico.ReadOnly = true;
            this.dataGridViewMedico.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewMedico.Size = new System.Drawing.Size(421, 184);
            this.dataGridViewMedico.TabIndex = 0;
            this.dataGridViewMedico.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewMedico_CellClick);
            // 
            // buttonPedirTurno
            // 
            this.buttonPedirTurno.Location = new System.Drawing.Point(457, 384);
            this.buttonPedirTurno.Name = "buttonPedirTurno";
            this.buttonPedirTurno.Size = new System.Drawing.Size(227, 46);
            this.buttonPedirTurno.TabIndex = 2;
            this.buttonPedirTurno.Text = "Pedir Turno";
            this.buttonPedirTurno.UseVisualStyleBackColor = true;
            this.buttonPedirTurno.Click += new System.EventHandler(this.buttonPedirTurno_Click);
            // 
            // monthCalendar1
            // 
            this.monthCalendar1.Location = new System.Drawing.Point(457, 32);
            this.monthCalendar1.MaxSelectionCount = 1;
            this.monthCalendar1.MinDate = new System.DateTime(2021, 11, 23, 0, 0, 0, 0);
            this.monthCalendar1.Name = "monthCalendar1";
            this.monthCalendar1.TabIndex = 3;
            this.monthCalendar1.DateChanged += new System.Windows.Forms.DateRangeEventHandler(this.monthCalendar1_DateChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(454, 332);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(61, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Hora Turno";
            // 
            // comboBoxHoraTurno
            // 
            this.comboBoxHoraTurno.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxHoraTurno.FormattingEnabled = true;
            this.comboBoxHoraTurno.Location = new System.Drawing.Point(457, 348);
            this.comboBoxHoraTurno.Name = "comboBoxHoraTurno";
            this.comboBoxHoraTurno.Size = new System.Drawing.Size(224, 21);
            this.comboBoxHoraTurno.TabIndex = 5;
            // 
            // labelSinHorarios
            // 
            this.labelSinHorarios.AutoSize = true;
            this.labelSinHorarios.Location = new System.Drawing.Point(545, 332);
            this.labelSinHorarios.Name = "labelSinHorarios";
            this.labelSinHorarios.Size = new System.Drawing.Size(136, 13);
            this.labelSinHorarios.TabIndex = 6;
            this.labelSinHorarios.Text = "No hay horarios disponibles";
            this.labelSinHorarios.Visible = false;
            // 
            // comboBoxEspecialidad
            // 
            this.comboBoxEspecialidad.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxEspecialidad.FormattingEnabled = true;
            this.comboBoxEspecialidad.Location = new System.Drawing.Point(457, 308);
            this.comboBoxEspecialidad.Name = "comboBoxEspecialidad";
            this.comboBoxEspecialidad.Size = new System.Drawing.Size(224, 21);
            this.comboBoxEspecialidad.TabIndex = 8;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(454, 292);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(67, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Especialidad";
            // 
            // labelGuardia
            // 
            this.labelGuardia.AutoSize = true;
            this.labelGuardia.Location = new System.Drawing.Point(454, 236);
            this.labelGuardia.Name = "labelGuardia";
            this.labelGuardia.Size = new System.Drawing.Size(109, 13);
            this.labelGuardia.TabIndex = 9;
            this.labelGuardia.Text = "Es medico de guardia";
            this.labelGuardia.Visible = false;
            // 
            // labelGuardia2
            // 
            this.labelGuardia2.AutoSize = true;
            this.labelGuardia2.Location = new System.Drawing.Point(454, 251);
            this.labelGuardia2.Name = "labelGuardia2";
            this.labelGuardia2.Size = new System.Drawing.Size(130, 13);
            this.labelGuardia2.TabIndex = 10;
            this.labelGuardia2.Text = "(Los turnos no se ocupan)";
            this.labelGuardia2.Visible = false;
            // 
            // FormSolicitarTurno
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(699, 452);
            this.Controls.Add(this.labelGuardia2);
            this.Controls.Add(this.labelGuardia);
            this.Controls.Add(this.comboBoxEspecialidad);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.labelSinHorarios);
            this.Controls.Add(this.comboBoxHoraTurno);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.monthCalendar1);
            this.Controls.Add(this.buttonPedirTurno);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "FormSolicitarTurno";
            this.Text = "FormSolicitarTurno";
            this.Load += new System.EventHandler(this.FormSolicitarTurno_Load);
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewPaciente)).EndInit();
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewMedico)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridView dataGridViewPaciente;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DataGridView dataGridViewMedico;
        private System.Windows.Forms.Button buttonPedirTurno;
        private System.Windows.Forms.MonthCalendar monthCalendar1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboBoxHoraTurno;
        private System.Windows.Forms.Label labelSinHorarios;
        private System.Windows.Forms.ComboBox comboBoxEspecialidad;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label labelGuardia;
        private System.Windows.Forms.Label labelGuardia2;
    }
}
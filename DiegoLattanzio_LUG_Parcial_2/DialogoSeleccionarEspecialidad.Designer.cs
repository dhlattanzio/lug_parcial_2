﻿
namespace DiegoLattanzio_LUG_Parcial_2
{
    partial class DialogoSeleccionarEspecialidad
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridViewEspecialidades = new System.Windows.Forms.DataGridView();
            this.buttonAgregar = new System.Windows.Forms.Button();
            this.buttonCancelar = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewEspecialidades)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridViewEspecialidades
            // 
            this.dataGridViewEspecialidades.AllowUserToAddRows = false;
            this.dataGridViewEspecialidades.AllowUserToDeleteRows = false;
            this.dataGridViewEspecialidades.AllowUserToResizeRows = false;
            this.dataGridViewEspecialidades.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewEspecialidades.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewEspecialidades.Location = new System.Drawing.Point(12, 12);
            this.dataGridViewEspecialidades.MultiSelect = false;
            this.dataGridViewEspecialidades.Name = "dataGridViewEspecialidades";
            this.dataGridViewEspecialidades.ReadOnly = true;
            this.dataGridViewEspecialidades.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewEspecialidades.Size = new System.Drawing.Size(427, 222);
            this.dataGridViewEspecialidades.TabIndex = 0;
            // 
            // buttonAgregar
            // 
            this.buttonAgregar.Location = new System.Drawing.Point(153, 240);
            this.buttonAgregar.Name = "buttonAgregar";
            this.buttonAgregar.Size = new System.Drawing.Size(140, 45);
            this.buttonAgregar.TabIndex = 1;
            this.buttonAgregar.Text = "Aceptar";
            this.buttonAgregar.UseVisualStyleBackColor = true;
            this.buttonAgregar.Click += new System.EventHandler(this.buttonAgregar_Click);
            // 
            // buttonCancelar
            // 
            this.buttonCancelar.Location = new System.Drawing.Point(299, 240);
            this.buttonCancelar.Name = "buttonCancelar";
            this.buttonCancelar.Size = new System.Drawing.Size(140, 45);
            this.buttonCancelar.TabIndex = 2;
            this.buttonCancelar.Text = "Cancelar";
            this.buttonCancelar.UseVisualStyleBackColor = true;
            this.buttonCancelar.Click += new System.EventHandler(this.buttonCancelar_Click);
            // 
            // DialogoSeleccionarEspecialidad
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(451, 297);
            this.Controls.Add(this.buttonCancelar);
            this.Controls.Add(this.buttonAgregar);
            this.Controls.Add(this.dataGridViewEspecialidades);
            this.Name = "DialogoSeleccionarEspecialidad";
            this.Text = "DialogoAgregarEspecialidad";
            this.Load += new System.EventHandler(this.DialogoAgregarEspecialidad_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewEspecialidades)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridViewEspecialidades;
        private System.Windows.Forms.Button buttonAgregar;
        private System.Windows.Forms.Button buttonCancelar;
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DiegoLattanzio_LUG_Parcial_2
{
    public partial class FormEstadisticasHospital : Form
    {
        private BLL.Especialidad bllEspecialidad;
        private BLL.HistoriaClinica bllHistoria;

        public FormEstadisticasHospital()
        {
            InitializeComponent();
            StartPosition = FormStartPosition.CenterParent;
        }

        private void FormEstadisticasHospital_Load(object sender, EventArgs e)
        {
            bllEspecialidad = new BLL.Especialidad();
            bllHistoria = new BLL.HistoriaClinica();

            ListarEspecialidades();
        }

        private void ListarEspecialidades()
        {
            List<BE.Especialidad> especialidades = new List<BE.Especialidad>();
            BE.Especialidad especialidadGeneral = new BE.Especialidad();
            especialidadGeneral.Id = -1;
            especialidadGeneral.Nombre = "General";
            especialidadGeneral.Estudios = new List<BE.Estudio>();
            especialidades.Add(especialidadGeneral);
            especialidades.AddRange(bllEspecialidad.Listar());

            dataGridView1.DataSource = especialidades;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            float ganancias = bllHistoria.ObtenerGananciaHospital();
            MessageBox.Show($"La ganancia total del hospital es de ${ganancias}");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            DialogoEspecialidadesInforme dialogo = new DialogoEspecialidadesInforme("ganancia");
            dialogo.ShowDialog();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            DialogoEspecialidadesInforme dialogo = new DialogoEspecialidadesInforme("cantidad");
            dialogo.ShowDialog();
        }
    }
}

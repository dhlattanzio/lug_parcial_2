﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DiegoLattanzio_LUG_Parcial_2
{
    public partial class FormEstadisticasPaciente : Form
    {
        private BLL.Paciente bllPaciente;
        private BLL.Especialidad bllEspecialidad;
        private BLL.HistoriaClinica bllHistoria;

        public FormEstadisticasPaciente()
        {
            InitializeComponent();
            StartPosition = FormStartPosition.CenterParent;
        }

        private void FormEstadisticasPaciente_Load(object sender, EventArgs e)
        {
            bllPaciente = new BLL.Paciente();
            bllEspecialidad = new BLL.Especialidad();
            bllHistoria = new BLL.HistoriaClinica();

            ListarPacientes();
            ListarEspecialidades();
        }

        private void ListarPacientes()
        {
            dataGridView1.DataSource = bllPaciente.Listar();
        }

        private void ListarEspecialidades()
        {
            List<BE.Especialidad> especialidades = new List<BE.Especialidad>();
            BE.Especialidad especialidadGeneral = new BE.Especialidad();
            especialidadGeneral.Id = -1;
            especialidadGeneral.Nombre = "General";
            especialidadGeneral.Estudios = new List<BE.Estudio>();
            especialidades.Add(especialidadGeneral);
            especialidades.AddRange(bllEspecialidad.Listar());

            comboBoxFiltroEspecialidad.DataSource = especialidades;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            BE.Paciente paciente = (BE.Paciente)dataGridView1.CurrentRow?.DataBoundItem;
            if (paciente!=null)
            {
                DialogoHistoriaPaciente dialogo = new DialogoHistoriaPaciente(paciente, null);
                dialogo.ShowDialog();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            BE.Paciente paciente = (BE.Paciente)dataGridView1.CurrentRow?.DataBoundItem;
            BE.Especialidad especialidad = (BE.Especialidad)comboBoxFiltroEspecialidad.SelectedItem;
            if (paciente != null & especialidad != null)
            {
                DialogoHistoriaPaciente dialogo = new DialogoHistoriaPaciente(paciente, especialidad);
                dialogo.ShowDialog();
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            BE.Paciente paciente = (BE.Paciente)dataGridView1.CurrentRow?.DataBoundItem;
            if (paciente != null)
            {
                float costoTotal = bllHistoria.ObtenerCostoTotal(paciente);
                MessageBox.Show($"El costo total que el paciente {paciente} debe pagar es de: ${costoTotal}");
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace DiegoLattanzio_LUG_Parcial_2
{
    public partial class DialogoEspecialidadesInforme : Form
    {
        private readonly string orden;

        public DialogoEspecialidadesInforme(string orden)
        {
            InitializeComponent();
            StartPosition = FormStartPosition.CenterParent;
            this.orden = orden;
        }

        private void DialogoEspecialidadesInforme_Load(object sender, EventArgs e)
        {
            BLL.HistoriaClinica historia = new BLL.HistoriaClinica();

            if (orden == "ganancia")
            {
                dataGridViewEspecialidades.DataSource = historia.ObtenerEspecialidadesOrdenadasPorGanancia();
            } else
            {
                dataGridViewEspecialidades.DataSource = historia.ObtenerEspecialidadesOrdenadasPorCantidadPacientes();
            }
        }

        private void GuardarInformeGanancias()
        {
            List<BE.InformeEspecialidadGanancia> informe = (List<BE.InformeEspecialidadGanancia>)dataGridViewEspecialidades.DataSource;
            XmlDocument doc = new XmlDocument();

            XmlDeclaration xmlDeclaration = doc.CreateXmlDeclaration("1.0", "UTF-8", null);
            XmlElement root = doc.DocumentElement;
            doc.InsertBefore(xmlDeclaration, root);

            XmlElement element1 = doc.CreateElement(string.Empty, "informe", string.Empty);
            doc.AppendChild(element1);

            foreach (BE.InformeEspecialidadGanancia inf in informe)
            {
                XmlElement element2 = doc.CreateElement(string.Empty, "especialidad", string.Empty);
                element1.AppendChild(element2);

                XmlElement element3 = doc.CreateElement(string.Empty, "nombre", string.Empty);
                XmlText text1 = doc.CreateTextNode(inf.Especialidad.Nombre);
                element3.AppendChild(text1);
                element2.AppendChild(element3);

                XmlElement element4 = doc.CreateElement(string.Empty, "ganancia", string.Empty);
                XmlText text2 = doc.CreateTextNode(inf.Ganancia.ToString());
                element4.AppendChild(text2);
                element2.AppendChild(element4);
            }

            doc.Save("D://informeEspecialidadesGanancias.xml");
        }

        private void GuardarInformeCantidad()
        {
            List<BE.InformeEspecialidadCantidad> informe = (List<BE.InformeEspecialidadCantidad>)dataGridViewEspecialidades.DataSource;
            XmlDocument doc = new XmlDocument();

            XmlDeclaration xmlDeclaration = doc.CreateXmlDeclaration("1.0", "UTF-8", null);
            XmlElement root = doc.DocumentElement;
            doc.InsertBefore(xmlDeclaration, root);

            XmlElement element1 = doc.CreateElement(string.Empty, "informe", string.Empty);
            doc.AppendChild(element1);

            foreach (BE.InformeEspecialidadCantidad inf in informe)
            {
                XmlElement element2 = doc.CreateElement(string.Empty, "especialidad", string.Empty);
                element1.AppendChild(element2);

                XmlElement element3 = doc.CreateElement(string.Empty, "nombre", string.Empty);
                XmlText text1 = doc.CreateTextNode(inf.Especialidad.Nombre);
                element3.AppendChild(text1);
                element2.AppendChild(element3);

                XmlElement element4 = doc.CreateElement(string.Empty, "cantidad", string.Empty);
                XmlText text2 = doc.CreateTextNode(inf.Cantidad.ToString());
                element4.AppendChild(text2);
                element2.AppendChild(element4);
            }

            doc.Save("D://informeEspecialidadesCantidad.xml");
        }

        private void buttonExportarXML_Click(object sender, EventArgs e)
        {
            try
            {
                if (orden == "ganancia")
                {
                    GuardarInformeGanancias();
                } else
                {
                    GuardarInformeCantidad();
                }
                MessageBox.Show("XML guardado en el disco D");
            } catch(Exception ex)
            {
                MessageBox.Show("Error al guardar informe: "+ex.Message);
            }
        }
    }
}

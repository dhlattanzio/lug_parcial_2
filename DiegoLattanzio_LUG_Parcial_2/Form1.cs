﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DiegoLattanzio_LUG_Parcial_2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            StartPosition = FormStartPosition.CenterScreen;
        }

        private void button1_Click(object sender, EventArgs e)
        {
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            Form formCargaEspecialidades = new FormCargaEspecialidades();
            formCargaEspecialidades.ShowDialog();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Form formCargaPacientes = new FormCargaPacientes();
            formCargaPacientes.ShowDialog();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Form formCargaMedicos = new FormCargaMedicos();
            formCargaMedicos.ShowDialog();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            FormEstadisticasHospital formEstadisticasHospital = new FormEstadisticasHospital();
            formEstadisticasHospital.ShowDialog();
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            Form formSolicitarTurno = new FormSolicitarTurno();
            formSolicitarTurno.ShowDialog();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            Form formAtenderPaciente = new FormAtenderPaciente();
            formAtenderPaciente.ShowDialog();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            FormEstadisticasPaciente formEstadisticasPaciente = new FormEstadisticasPaciente();
            formEstadisticasPaciente.ShowDialog();
        }

        private void button8_Click(object sender, EventArgs e)
        {
            FormHacerEstudio formHacerEstudio = new FormHacerEstudio();
            formHacerEstudio.ShowDialog();
        }
    }
}

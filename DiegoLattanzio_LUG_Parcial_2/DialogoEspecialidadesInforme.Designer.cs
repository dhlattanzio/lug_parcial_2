﻿
namespace DiegoLattanzio_LUG_Parcial_2
{
    partial class DialogoEspecialidadesInforme
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonExportarXML = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dataGridViewEspecialidades = new System.Windows.Forms.DataGridView();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewEspecialidades)).BeginInit();
            this.SuspendLayout();
            // 
            // buttonExportarXML
            // 
            this.buttonExportarXML.Location = new System.Drawing.Point(329, 273);
            this.buttonExportarXML.Name = "buttonExportarXML";
            this.buttonExportarXML.Size = new System.Drawing.Size(173, 52);
            this.buttonExportarXML.TabIndex = 3;
            this.buttonExportarXML.Text = "Exportar a XML";
            this.buttonExportarXML.UseVisualStyleBackColor = true;
            this.buttonExportarXML.Click += new System.EventHandler(this.buttonExportarXML_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.dataGridViewEspecialidades);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(496, 261);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Especialidades";
            // 
            // dataGridViewEspecialidades
            // 
            this.dataGridViewEspecialidades.AllowUserToAddRows = false;
            this.dataGridViewEspecialidades.AllowUserToDeleteRows = false;
            this.dataGridViewEspecialidades.AllowUserToResizeRows = false;
            this.dataGridViewEspecialidades.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewEspecialidades.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewEspecialidades.Location = new System.Drawing.Point(7, 20);
            this.dataGridViewEspecialidades.MultiSelect = false;
            this.dataGridViewEspecialidades.Name = "dataGridViewEspecialidades";
            this.dataGridViewEspecialidades.ReadOnly = true;
            this.dataGridViewEspecialidades.Size = new System.Drawing.Size(483, 235);
            this.dataGridViewEspecialidades.TabIndex = 0;
            // 
            // DialogoEspecialidadesInforme
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(519, 339);
            this.Controls.Add(this.buttonExportarXML);
            this.Controls.Add(this.groupBox1);
            this.Name = "DialogoEspecialidadesInforme";
            this.Text = "DialogoEspecialidadesInforme";
            this.Load += new System.EventHandler(this.DialogoEspecialidadesInforme_Load);
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewEspecialidades)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonExportarXML;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridView dataGridViewEspecialidades;
    }
}
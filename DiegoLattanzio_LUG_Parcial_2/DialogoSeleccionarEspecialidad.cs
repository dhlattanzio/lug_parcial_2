﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DiegoLattanzio_LUG_Parcial_2
{
    public partial class DialogoSeleccionarEspecialidad : Form
    {
        private BLL.Especialidad bllEspecialidad;

        public BE.Especialidad EspecialidadSeleccionada { get; set; } = null;

        private readonly IEnumerable<BE.Especialidad> listaIgnorarEspecialidades;

        public DialogoSeleccionarEspecialidad(IEnumerable<BE.Especialidad> listaIgnorarEspecialidades)
        {
            InitializeComponent();
            StartPosition = FormStartPosition.CenterParent;
            this.listaIgnorarEspecialidades = listaIgnorarEspecialidades;
        }

        private void DialogoAgregarEspecialidad_Load(object sender, EventArgs e)
        {
            bllEspecialidad = new BLL.Especialidad();
            CargarEspecialidades();
        }

        private void CargarEspecialidades()
        {
            dataGridViewEspecialidades.DataSource = bllEspecialidad.Listar().Where(x => !listaIgnorarEspecialidades.Any(e => x.Id == e.Id)).ToList();
        }

        private void buttonAgregar_Click(object sender, EventArgs e)
        {
            EspecialidadSeleccionada = (BE.Especialidad)dataGridViewEspecialidades.CurrentRow?.DataBoundItem;
            if (EspecialidadSeleccionada != null)
            {
                DialogResult = DialogResult.OK;

            } else
            {
                DialogResult = DialogResult.Abort;
            }

            Close();
        }

        private void buttonCancelar_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }
    }
}

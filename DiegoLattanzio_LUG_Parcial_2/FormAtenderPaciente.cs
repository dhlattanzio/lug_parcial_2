﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DiegoLattanzio_LUG_Parcial_2
{
    public partial class FormAtenderPaciente : Form
    {
        private BLL.Turno bllTurno;
        private BLL.HistoriaClinica bllHistoria;
        private BLL.Derivacion bllDerivacion;
        private BLL.OrdenMedica bllOrdenMedica;

        public FormAtenderPaciente()
        {
            InitializeComponent();
            StartPosition = FormStartPosition.CenterParent;
        }

        private void FormAtenderPaciente_Load(object sender, EventArgs e)
        {
            bllTurno = new BLL.Turno();
            bllHistoria = new BLL.HistoriaClinica();
            bllDerivacion = new BLL.Derivacion();
            bllOrdenMedica = new BLL.OrdenMedica();

            ListarTurnos();
        }

        private void ListarTurnos()
        {
            dataGridViewTurnos.DataSource = null;
            dataGridViewTurnos.DataSource = bllTurno.Listar()
                .Where(x => !x.FueAtendido)
                .ToList();
        }

        private void DiagnosticarPaciente(BE.Turno turno)
        {
            DialogoDiagnosticarPaciente dialogo = new DialogoDiagnosticarPaciente();
            DialogResult result = dialogo.ShowDialog();
            if (result == DialogResult.OK)
            {
                string detalles = dialogo.Diagnostico;

                BE.Historia historia = bllHistoria.CrearNuevaHistoria(turno);
                historia.Detalles = detalles;

                bllTurno.MarcarTurnoComoAtendido(turno);
                bllHistoria.Grabar(historia);
                ListarTurnos();

                MessageBox.Show("Paciente diagnosticado con exito.");
            }
        }

        private void DerivarPaciente(BE.Turno turno)
        {
            List<BE.Especialidad> ignorar = new List<BE.Especialidad>();
            ignorar.Add(turno.Especialidad);

            DialogoSeleccionarEspecialidad dialogo = new DialogoSeleccionarEspecialidad(ignorar);
            DialogResult result = dialogo.ShowDialog();
            if (result == DialogResult.OK)
            {
                BE.Especialidad derivar = dialogo.EspecialidadSeleccionada;

                BE.Historia historia = bllHistoria.CrearNuevaHistoria(turno);
                historia.Detalles = $"Derivado a {derivar}";

                BE.Derivacion derivacion = new BE.Derivacion();
                derivacion.Turno = turno;
                derivacion.Especialidad = derivar;
                derivacion.Fecha = DateTime.Now;

                bllDerivacion.Grabar(derivacion);
                bllTurno.MarcarTurnoComoAtendido(turno);
                bllHistoria.Grabar(historia);
                ListarTurnos();

                MessageBox.Show("Paciente derivado con exito.");
            }
        }

        private void SolicitarEstudio(BE.Turno turno)
        {
            DialogoPedirEstudio dialogo = new DialogoPedirEstudio();
            DialogResult result = dialogo.ShowDialog();
            if (result == DialogResult.OK)
            {
                BE.Historia historia = bllHistoria.CrearNuevaHistoria(turno);
                historia.Detalles = $"Estudio solicitado {dialogo.Estudio.Nombre}";

                BE.OrdenMedica orden = new BE.OrdenMedica();
                orden.Medico = turno.Medico;
                orden.Paciente = turno.Paciente;
                orden.Estudio = dialogo.Estudio;
                orden.Fecha = turno.Fecha;

                bllOrdenMedica.Grabar(orden);

                bllTurno.MarcarTurnoComoAtendido(turno);
                bllHistoria.Grabar(historia);
                ListarTurnos();

                MessageBox.Show("Estudio solicitado con extio.");
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            BE.Turno turno = (BE.Turno)dataGridViewTurnos.CurrentRow?.DataBoundItem;
            if (turno != null)
            {
                DiagnosticarPaciente(turno);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            BE.Turno turno = (BE.Turno)dataGridViewTurnos.CurrentRow?.DataBoundItem;
            if (turno != null)
            {
                DerivarPaciente(turno);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            BE.Turno turno = (BE.Turno)dataGridViewTurnos.CurrentRow?.DataBoundItem;
            if (turno != null)
            {
                SolicitarEstudio(turno);
            }
        }
    }
}

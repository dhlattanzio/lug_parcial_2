USE [master]
GO
/****** Object:  Database [LUG_Lattanzio_Parcial2]    Script Date: 11/23/2021 23:13:15 ******/
CREATE DATABASE [LUG_Lattanzio_Parcial2]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'LUG_Lattanzio_Parcial2', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPRESS\MSSQL\DATA\LUG_Lattanzio_Parcial2.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'LUG_Lattanzio_Parcial2_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPRESS\MSSQL\DATA\LUG_Lattanzio_Parcial2_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [LUG_Lattanzio_Parcial2] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [LUG_Lattanzio_Parcial2].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [LUG_Lattanzio_Parcial2] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [LUG_Lattanzio_Parcial2] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [LUG_Lattanzio_Parcial2] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [LUG_Lattanzio_Parcial2] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [LUG_Lattanzio_Parcial2] SET ARITHABORT OFF 
GO
ALTER DATABASE [LUG_Lattanzio_Parcial2] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [LUG_Lattanzio_Parcial2] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [LUG_Lattanzio_Parcial2] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [LUG_Lattanzio_Parcial2] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [LUG_Lattanzio_Parcial2] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [LUG_Lattanzio_Parcial2] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [LUG_Lattanzio_Parcial2] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [LUG_Lattanzio_Parcial2] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [LUG_Lattanzio_Parcial2] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [LUG_Lattanzio_Parcial2] SET  DISABLE_BROKER 
GO
ALTER DATABASE [LUG_Lattanzio_Parcial2] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [LUG_Lattanzio_Parcial2] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [LUG_Lattanzio_Parcial2] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [LUG_Lattanzio_Parcial2] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [LUG_Lattanzio_Parcial2] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [LUG_Lattanzio_Parcial2] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [LUG_Lattanzio_Parcial2] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [LUG_Lattanzio_Parcial2] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [LUG_Lattanzio_Parcial2] SET  MULTI_USER 
GO
ALTER DATABASE [LUG_Lattanzio_Parcial2] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [LUG_Lattanzio_Parcial2] SET DB_CHAINING OFF 
GO
ALTER DATABASE [LUG_Lattanzio_Parcial2] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [LUG_Lattanzio_Parcial2] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [LUG_Lattanzio_Parcial2] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [LUG_Lattanzio_Parcial2] SET ACCELERATED_DATABASE_RECOVERY = OFF  
GO
ALTER DATABASE [LUG_Lattanzio_Parcial2] SET QUERY_STORE = OFF
GO
USE [LUG_Lattanzio_Parcial2]
GO
/****** Object:  Table [dbo].[derivaciones]    Script Date: 11/23/2021 23:13:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[derivaciones](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[idTurno] [int] NOT NULL,
	[idEspecialidad] [int] NOT NULL,
	[fecha] [datetime] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[especialidades]    Script Date: 11/23/2021 23:13:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[especialidades](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[nombre] [varchar](50) NOT NULL,
 CONSTRAINT [PK_especialidades] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[estudios]    Script Date: 11/23/2021 23:13:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[estudios](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[idEspecialidad] [int] NOT NULL,
	[nombre] [varchar](50) NOT NULL,
	[costo] [float] NOT NULL,
 CONSTRAINT [PK_estudios] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[historia]    Script Date: 11/23/2021 23:13:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[historia](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[idMedico] [int] NOT NULL,
	[idPaciente] [int] NOT NULL,
	[idEspecialidad] [int] NOT NULL,
	[fecha] [datetime] NOT NULL,
	[costo] [float] NOT NULL,
	[detalles] [text] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[medicoEspecialidad]    Script Date: 11/23/2021 23:13:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[medicoEspecialidad](
	[idMedico] [int] NOT NULL,
	[idEspecialidad] [int] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[medicos]    Script Date: 11/23/2021 23:13:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[medicos](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[nombre] [varchar](50) NOT NULL,
	[apellido] [varchar](50) NOT NULL,
	[cuil] [int] NOT NULL,
	[costo] [float] NOT NULL,
	[fechaNacimiento] [datetime] NOT NULL,
	[esMedicoGuardia] [bit] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ordenes]    Script Date: 11/23/2021 23:13:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ordenes](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[idMedico] [int] NOT NULL,
	[idPaciente] [int] NOT NULL,
	[idEstudio] [int] NOT NULL,
	[fecha] [datetime] NOT NULL,
	[fueHecho] [bit] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[pacientes]    Script Date: 11/23/2021 23:13:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[pacientes](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[nombre] [varchar](50) NOT NULL,
	[apellido] [varchar](50) NOT NULL,
	[dni] [int] NOT NULL,
	[fechaNacimiento] [date] NOT NULL,
 CONSTRAINT [PK_pacientes] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[turnos]    Script Date: 11/23/2021 23:13:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[turnos](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[idMedico] [int] NOT NULL,
	[idPaciente] [int] NOT NULL,
	[idEspecialidad] [int] NOT NULL,
	[fecha] [datetime] NOT NULL,
	[numTurno] [int] NOT NULL,
	[fueAtendido] [bit] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ordenes] ADD  CONSTRAINT [DF_ordenes_fueHecho]  DEFAULT ((0)) FOR [fueHecho]
GO
ALTER TABLE [dbo].[turnos] ADD  CONSTRAINT [DF_turnos_idEspecialidad]  DEFAULT ((-1)) FOR [idEspecialidad]
GO
ALTER TABLE [dbo].[turnos] ADD  CONSTRAINT [DF_turnos_fueAtendido]  DEFAULT ((0)) FOR [fueAtendido]
GO
/****** Object:  StoredProcedure [dbo].[AGREGAR_MEDICO_ESPECIALIDAD]    Script Date: 11/23/2021 23:13:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[AGREGAR_MEDICO_ESPECIALIDAD]
@idMedico int,
@idEspecialidad int
AS
BEGIN
INSERT INTO dbo.medicoEspecialidad(idMedico, idEspecialidad) VALUES(@idMedico, @idEspecialidad);
END
GO
/****** Object:  StoredProcedure [dbo].[BORRAR_ESPECIALIDAD]    Script Date: 11/23/2021 23:13:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[BORRAR_ESPECIALIDAD]
@id int
AS
BEGIN
DELETE FROM dbo.especialidades WHERE id = @id
END
GO
/****** Object:  StoredProcedure [dbo].[BORRAR_ESTUDIO]    Script Date: 11/23/2021 23:13:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[BORRAR_ESTUDIO]
@id int
AS
BEGIN
DELETE FROM dbo.estudios WHERE id = @id
END
GO
/****** Object:  StoredProcedure [dbo].[BORRAR_MEDICO]    Script Date: 11/23/2021 23:13:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[BORRAR_MEDICO]
@id int
AS
BEGIN
DELETE FROM dbo.medicos WHERE id = @id;
DELETE FROM dbo.medicoEspecialidad WHERE idMedico = @id;
END
GO
/****** Object:  StoredProcedure [dbo].[BORRAR_MEDICO_ESPECIALIDADES]    Script Date: 11/23/2021 23:13:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[BORRAR_MEDICO_ESPECIALIDADES]
@id int
as
begin
delete from dbo.medicoEspecialidad where idMedico=@id;
end
GO
/****** Object:  StoredProcedure [dbo].[BORRAR_PACIENTE]    Script Date: 11/23/2021 23:13:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[BORRAR_PACIENTE]
@id int
AS
BEGIN
DELETE FROM dbo.pacientes WHERE id = @id;
END
GO
/****** Object:  StoredProcedure [dbo].[BORRAR_TURNO]    Script Date: 11/23/2021 23:13:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[BORRAR_TURNO]
@id int
AS
BEGIN
delete from dbo.turnos where id=@id;
END
GO
/****** Object:  StoredProcedure [dbo].[EDITAR_ESPECIALIDAD]    Script Date: 11/23/2021 23:13:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[EDITAR_ESPECIALIDAD]
@nombre varchar(50)
AS
BEGIN
UPDATE dbo.especialidades SET nombre=@nombre;
END
GO
/****** Object:  StoredProcedure [dbo].[EDITAR_ESTUDIO]    Script Date: 11/23/2021 23:13:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[EDITAR_ESTUDIO]
@nombre varchar(50),
@costo float
AS
BEGIN
UPDATE dbo.estudios SET nombre=@nombre, costo=@costo;
END
GO
/****** Object:  StoredProcedure [dbo].[EDITAR_MEDICO]    Script Date: 11/23/2021 23:13:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[EDITAR_MEDICO]
@nombre varchar(50),
@apellido varchar(50),
@cuil int,
@costo float,
@fechaNacimiento datetime,
@esMedicoGuardia bit
AS
BEGIN
UPDATE dbo.medicos SET nombre=@nombre, apellido=@apellido, cuil=@cuil, costo=@costo, fechaNacimiento=@fechaNacimiento, esMedicoGuardia=@esMedicoGuardia;
END
GO
/****** Object:  StoredProcedure [dbo].[EDITAR_ORDEN]    Script Date: 11/23/2021 23:13:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[EDITAR_ORDEN]
@id int,
@idMedico int,
@idPaciente int,
@idEstudio int,
@fecha datetime,
@fueHecho bit
AS
BEGIN
UPDATE dbo.ordenes SET idMedico=@idMedico, idPaciente=@idPaciente, idEstudio=@idEstudio, fecha=@fecha, fueHecho=@fueHecho WHERE id=@id;
END
GO
/****** Object:  StoredProcedure [dbo].[EDITAR_PACIENTE]    Script Date: 11/23/2021 23:13:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[EDITAR_PACIENTE]
@id int,
@nombre varchar(50),
@apellido varchar(50),
@dni int,
@fechaNacimiento datetime
AS
BEGIN
UPDATE dbo.pacientes SET nombre=@nombre, apellido=@apellido, dni=@dni, fechaNacimiento=@fechaNacimiento WHERE id=@id;
END
GO
/****** Object:  StoredProcedure [dbo].[EDITAR_TURNO]    Script Date: 11/23/2021 23:13:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[EDITAR_TURNO]
@id int,
@idMedico int,
@idPaciente int,
@idEspecialidad int,
@fecha datetime,
@numTurno int,
@fueAtendido bit
AS
BEGIN
UPDATE dbo.turnos SET idMedico=@idMedico, idPaciente=@idPaciente, idEspecialidad=@idEspecialidad, fecha=@fecha, numTurno=@numTurno, fueAtendido=@fueAtendido WHERE id=@id;
END
GO
/****** Object:  StoredProcedure [dbo].[INSERTAR_DERIVACION]    Script Date: 11/23/2021 23:13:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[INSERTAR_DERIVACION] 
@idTurno int,
@idEspecialidad int,
@fecha datetime
AS
BEGIN
INSERT INTO dbo.derivaciones(idTurno, idEspecialidad, fecha) VALUES (@idTurno, @idEspecialidad, @fecha);
END
GO
/****** Object:  StoredProcedure [dbo].[INSERTAR_ESPECIALIDAD]    Script Date: 11/23/2021 23:13:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[INSERTAR_ESPECIALIDAD]
@nombre varchar(50)
AS
BEGIN
INSERT INTO dbo.especialidades(nombre) VALUES (@nombre);
END
GO
/****** Object:  StoredProcedure [dbo].[INSERTAR_ESTUDIO]    Script Date: 11/23/2021 23:13:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[INSERTAR_ESTUDIO]
@idEspecialidad int,
@nombre varchar(50),
@costo float
AS
BEGIN
INSERT INTO dbo.estudios(idEspecialidad, nombre, costo) VALUES (@idEspecialidad, @nombre, @costo);
END
GO
/****** Object:  StoredProcedure [dbo].[INSERTAR_HISTORIA]    Script Date: 11/23/2021 23:13:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[INSERTAR_HISTORIA]
@idMedico int,
@idPaciente int,
@idEspecialidad int,
@fecha datetime,
@costo float,
@detalles text
AS
BEGIN
INSERT INTO dbo.historia(idMedico, idPaciente, idEspecialidad, fecha, costo, detalles) VALUES (@idMedico, @idPaciente, @idEspecialidad, @fecha, @costo, @detalles);
END
GO
/****** Object:  StoredProcedure [dbo].[INSERTAR_MEDICO]    Script Date: 11/23/2021 23:13:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[INSERTAR_MEDICO]
@nombre varchar(50),
@apellido varchar(50),
@cuil int,
@costo float,
@fechaNacimiento datetime,
@esMedicoGuardia bit
AS
BEGIN
INSERT INTO dbo.medicos(nombre, apellido, cuil, costo, fechaNacimiento, esMedicoGuardia) VALUES (@nombre, @apellido, @cuil, @costo, @fechaNacimiento, @esMedicoGuardia);
END
GO
/****** Object:  StoredProcedure [dbo].[INSERTAR_ORDEN]    Script Date: 11/23/2021 23:13:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[INSERTAR_ORDEN]
@idMedico int,
@idPaciente int,
@idEstudio int,
@fecha datetime
AS
BEGIN
INSERT INTO dbo.ordenes(idMedico, idPaciente, idEstudio, fecha) VALUES(@idMedico, @idPaciente, @idEstudio, @fecha);
END
GO
/****** Object:  StoredProcedure [dbo].[INSERTAR_PACIENTE]    Script Date: 11/23/2021 23:13:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[INSERTAR_PACIENTE]
@nombre varchar(50),
@apellido varchar(50),
@dni int,
@fechaNacimiento datetime
AS
BEGIN
INSERT INTO dbo.pacientes(nombre, apellido, dni, fechaNacimiento) VALUES (@nombre, @apellido, @dni, @fechaNacimiento)
END
GO
/****** Object:  StoredProcedure [dbo].[INSERTAR_TURNO]    Script Date: 11/23/2021 23:13:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[INSERTAR_TURNO]
@idMedico int,
@idPaciente int,
@idEspecialidad int,
@fecha datetime,
@numTurno int
AS
BEGIN
INSERT INTO dbo.turnos(idMedico, idPaciente, idEspecialidad, fecha, numTurno) VALUES (@idMedico, @idPaciente, @idEspecialidad, @fecha, @numTurno);
END
GO
/****** Object:  StoredProcedure [dbo].[LIMPIAR_DB]    Script Date: 11/23/2021 23:13:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[LIMPIAR_DB]
AS
BEGIN
TRUNCATE TABLE dbo.derivaciones;
TRUNCATE TABLE dbo.especialidades;
TRUNCATE TABLE dbo.estudios;
TRUNCATE TABLE dbo.historia;
TRUNCATE TABLE dbo.medicoEspecialidad;
TRUNCATE TABLE dbo.medicos;
TRUNCATE TABLE dbo.pacientes;
TRUNCATE TABLE dbo.turnos;
END
GO
/****** Object:  StoredProcedure [dbo].[LISTAR_DERIVACION]    Script Date: 11/23/2021 23:13:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[LISTAR_DERIVACION] 
AS
BEGIN
SELECT * FROM dbo.derivaciones;
END
GO
/****** Object:  StoredProcedure [dbo].[LISTAR_ESPECIALIDADES]    Script Date: 11/23/2021 23:13:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[LISTAR_ESPECIALIDADES]
AS
SELECT * FROM dbo.especialidades;
GO
/****** Object:  StoredProcedure [dbo].[LISTAR_ESTUDIOS]    Script Date: 11/23/2021 23:13:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[LISTAR_ESTUDIOS]
AS
SELECT * FROM dbo.estudios;
GO
/****** Object:  StoredProcedure [dbo].[LISTAR_HISTORIA]    Script Date: 11/23/2021 23:13:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[LISTAR_HISTORIA]
AS
BEGIN
select * from dbo.historia;
END
GO
/****** Object:  StoredProcedure [dbo].[LISTAR_MEDICO]    Script Date: 11/23/2021 23:13:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[LISTAR_MEDICO]
AS
BEGIN
SELECT * FROM dbo.medicos;
END
GO
/****** Object:  StoredProcedure [dbo].[LISTAR_MEDICO_ESPECIALIDAD]    Script Date: 11/23/2021 23:13:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[LISTAR_MEDICO_ESPECIALIDAD]
as
begin
SELECT * from dbo.medicoEspecialidad;
end
GO
/****** Object:  StoredProcedure [dbo].[LISTAR_ORDENES]    Script Date: 11/23/2021 23:13:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[LISTAR_ORDENES]
AS
BEGIN
SELECT * FROM dbo.ordenes;
END
GO
/****** Object:  StoredProcedure [dbo].[LISTAR_PACIENTES]    Script Date: 11/23/2021 23:13:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[LISTAR_PACIENTES] 
as
select * from dbo.pacientes;
GO
/****** Object:  StoredProcedure [dbo].[LISTAR_TURNO]    Script Date: 11/23/2021 23:13:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[LISTAR_TURNO]
AS
BEGIN
SELECT * FROM dbo.turnos;
END
GO
/****** Object:  StoredProcedure [dbo].[OBTENER_MEDICO_POR_CUIL]    Script Date: 11/23/2021 23:13:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[OBTENER_MEDICO_POR_CUIL]
@cuil int
as
begin
select * from dbo.medicos where cuil=@cuil;
end
GO
USE [master]
GO
ALTER DATABASE [LUG_Lattanzio_Parcial2] SET  READ_WRITE 
GO

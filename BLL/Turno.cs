﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class Turno
    {
        private readonly DAL.MP_Turno mpTurno = new DAL.MP_Turno();

        public List<BE.Turno> Listar()
        {
            return mpTurno.Listar();
        }

        public bool Grabar(BE.Turno turno)
        {
            return mpTurno.Insertar(turno) >= 0;
        }

        public void MarcarTurnoComoAtendido(BE.Turno turno)
        {
            turno.FueAtendido = true;
            mpTurno.Editar(turno);
        }
    }
}

﻿using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class HistoriaClinica
    {
        private readonly MP_HistoriaClinica mpHistoriaClinica = new MP_HistoriaClinica();
        private readonly BLL.Medico bllMedico = new Medico();

        public BE.Historia CrearNuevaHistoria(BE.Turno turno)
        {
            BE.Historia historia = new BE.Historia();
            historia.Medico = turno.Medico;
            historia.Paciente = turno.Paciente;
            historia.Especialidad = turno.Especialidad;
            historia.Fecha = turno.Fecha;
            historia.Costo = bllMedico.ObtenerCosto(turno.Medico);
            historia.Detalles = "?";
            return historia;
        }

        public bool Grabar(BE.Historia historia)
        {
            return mpHistoriaClinica.Insertar(historia) >= 0;
        }

        public List<BE.Historia> ObtenerHistoriasFormaDescendienteFecha(BE.Paciente paciente)
        {
            return mpHistoriaClinica.Listar()
                .Where(x => x.Paciente.Id == paciente.Id)
                .OrderBy(x => x.Fecha).Reverse()
                .ToList();
        }

        public List<BE.Historia> ObtenerHistoriasFiltradaEspecialidad(BE.Paciente paciente,
            BE.Especialidad especialidad)
        {
            return mpHistoriaClinica.Listar()
                .Where(x => x.Paciente.Id == paciente.Id)
                .Where(x => x.Especialidad.Id == especialidad.Id)
                .ToList();
        }

        public float ObtenerCostoTotal(BE.Paciente paciente)
        {
            List<BE.Historia> historias = mpHistoriaClinica.Listar()
                .Where(x => x.Paciente.Id == paciente.Id).ToList();

            float costo = 0;
            foreach(BE.Historia h in historias)
            {
                costo += h.Costo;
            }
            return costo;
        }

        public float ObtenerGananciaHospital()
        {
            List<BE.Historia> historias = mpHistoriaClinica.Listar();
            float ganancias = 0;
            foreach(BE.Historia h in historias)
            {
                ganancias += h.Costo;
            }
            return ganancias;
        }

        public List<BE.InformeEspecialidadGanancia> ObtenerEspecialidadesOrdenadasPorGanancia()
        {
            List<BE.InformeEspecialidadGanancia> informe = new List<BE.InformeEspecialidadGanancia>();

            foreach(BE.Historia historia in mpHistoriaClinica.Listar())
            {
                if (informe.Any(x => x.Especialidad.Id == historia.Especialidad.Id))
                {
                    BE.InformeEspecialidadGanancia esp = informe.Single(x => x.Especialidad.Id == historia.Especialidad.Id);
                    esp.Ganancia += historia.Costo;
                } else
                {
                    BE.InformeEspecialidadGanancia esp = new BE.InformeEspecialidadGanancia();
                    esp.Especialidad = historia.Especialidad;
                    esp.Ganancia += historia.Costo;
                    informe.Add(esp);
                }
            }

            informe.Reverse();
            return informe;
        }

        public List<BE.InformeEspecialidadCantidad> ObtenerEspecialidadesOrdenadasPorCantidadPacientes()
        {
            List<BE.InformeEspecialidadCantidad> informe = new List<BE.InformeEspecialidadCantidad>();

            foreach (BE.Historia historia in mpHistoriaClinica.Listar())
            {
                if (informe.Any(x => x.Especialidad.Id == historia.Especialidad.Id))
                {
                    BE.InformeEspecialidadCantidad esp = informe.Single(x => x.Especialidad.Id == historia.Especialidad.Id);
                    esp.Cantidad += 1;
                }
                else
                {
                    BE.InformeEspecialidadCantidad esp = new BE.InformeEspecialidadCantidad();
                    esp.Especialidad = historia.Especialidad;
                    esp.Cantidad += 1;
                    informe.Add(esp);
                }
            }

            informe.Reverse();
            return informe;
        }
    }
}

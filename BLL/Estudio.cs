﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class Estudio
    {
        private readonly DAL.MP_Estudios mpEstudios = new DAL.MP_Estudios();

        public List<BE.Estudio> Listar()
        {
            return mpEstudios.Listar();
        }
    }
}

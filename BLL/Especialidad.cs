﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class Especialidad
    {
        private DAL.MP_Especialidad mpEspecialidad = new DAL.MP_Especialidad();
        private readonly DAL.MP_HistoriaClinica mpHistoriaClinica = new DAL.MP_HistoriaClinica();

        public List<BE.Especialidad> Listar()
        {
            return mpEspecialidad.Listar();
        }

        public bool Grabar(BE.Especialidad especialidad)
        {
            return mpEspecialidad.Insertar(especialidad) > 0;
        }

        public bool Borrar(BE.Especialidad especialidad)
        {
            return mpEspecialidad.Borrar(especialidad) > 0;
        }

        public List<BE.Especialidad> ObtenerEspecialidadesOrdenadasPorGanancias()
        {
            return null;
        }

        public List<BE.Especialidad> ObtenerEspecialidadesOrdenadasPorCantidadPacientes()
        {
            return null;
        }

        public BE.Especialidad ObtenerEspecialidad(int id)
        {
            return Listar().SingleOrDefault(x => x.Id == id);
        }
    }
}

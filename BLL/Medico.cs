﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class Medico
    {
        private readonly DAL.MP_Medico mpMedico = new DAL.MP_Medico();

        public bool Grabar(BE.Medico medico)
        {
            return mpMedico.Insertar(medico) >= 0;
        }
        
        public bool Borrar(BE.Medico medico)
        {
            return mpMedico.Borrar(medico) >= 0;
        }

        public bool Editar(BE.Medico medico)
        {
            return mpMedico.Editar(medico) >= 0;
        }

        public List<BE.Medico> ListarMedicos()
        {
            return mpMedico.Listar();
        }

        public float ObtenerCosto(BE.Medico medico)
        {
            return medico.Costo + (medico.Costo * (0.15f * medico.Especialidades.Count));
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class Paciente
    {
        private DAL.MP_Paciente mpPaciente = new DAL.MP_Paciente();

        public List<BE.Paciente> Listar()
        {
            return mpPaciente.Listar();
        }

        public bool Grabar(BE.Paciente paciente)
        {
            return mpPaciente.Insertar(paciente) >= 0;
        }

        public bool Borrar(BE.Paciente paciente)
        {
            return mpPaciente.Borrar(paciente) >= 0;
        }

        public bool Editar(BE.Paciente paciente)
        {
            return mpPaciente.Editar(paciente) >= 0;
        }
    }
}

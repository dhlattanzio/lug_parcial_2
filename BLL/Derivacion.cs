﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class Derivacion
    {
        private readonly DAL.MP_Derivacion mpDerivacion = new DAL.MP_Derivacion();

        public bool Grabar(BE.Derivacion derivacion)
        {
            return mpDerivacion.Insertar(derivacion) >= 0;
        }

        public List<BE.Derivacion> Listar()
        {
            return mpDerivacion.Listar();
        }
    }
}

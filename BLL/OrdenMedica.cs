﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class OrdenMedica
    {
        private readonly DAL.MP_OrdenMedica mpOrdenMedica = new DAL.MP_OrdenMedica();

        public List<BE.OrdenMedica> Listar()
        {
            return mpOrdenMedica.Listar();
        }

        public bool Editar(BE.OrdenMedica orden)
        {
            return mpOrdenMedica.Editar(orden) >= 0;
        }

        public bool Grabar(BE.OrdenMedica orden)
        {
            return mpOrdenMedica.Insertar(orden) >= 0;
        }
    }
}
